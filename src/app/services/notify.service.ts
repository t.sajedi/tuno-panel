import { Injectable } from '@angular/core';
import { forkJoin } from "rxjs";

import { NotificationsService, NotificationType as Notify } from 'angular2-notifications';
export { Notify };
import { TranslateService } from '@ngx-translate/core';
import Swal, { SweetAlertOptions, SweetAlertResult } from 'sweetalert2';

export interface AlertOptions extends SweetAlertOptions {
  confirmButtonClass?: string;
  cancelButtonClass?: string;
}

export enum Opr {
  Create = "create",
  Select = "select",
  Del = "del",
  Remove = "remove",
  Upload = "upload",
  Save = "save",
  Edit = "edit"
}

export enum Ent {
  Order = "order",
  Contact = "contact",
  Comment = "comment",
  Image = "image",
  Plan = "plan",
  UserProfile = "user-profile",
  Setting = "Setting",
  artist = "artist",
  Discount_code = "discount-code",
  Banner = "banner",
  Access = "access",
  Video = "video",
  Actor = "actor",
  Director = "director",
  Preview = "preview",
  Genre = "genre",
  Tag = "tag",
  Age = "age",
  Category = "category",
  CategoryData = "category-data",
  MainPage = "main-page",
  Blog = "blog"
}

export enum SwlT {
  Success = "success",
  Warn = "warning",
  Error = "error",
  Info = "info",
  Question = "question",
}

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  constructor(
    private _notify: NotificationsService,
    private _translate: TranslateService,
  ) { }

  public status(
    operation: Opr,
    entity: Ent,
    name?: string,
    type: Notify = Notify.Success,
    textKey: string = "success"
  ): void {
    let operation$ = this._translate.get(`operation.${operation}`);
    let entity$ = this._translate.get(`entity.${entity}`);
    forkJoin([operation$, entity$]).subscribe(data1 => {
      const [operation, entity]: string[] = data1;
      let statusTitle$ = this._translate.get(`notify.${textKey}.title`, { operation: operation, entity: entity });
      let statusMessage$ = this._translate.get(`notify.${textKey}.message`, { operation: operation, name: name || entity });
      forkJoin([statusTitle$, statusMessage$]).subscribe(data2 => {
        let [title, message]: string[] = data2;
        this._notify.create(title, message, type);
      });
    });
  }

  public create(title: string, message: string, type: Notify = Notify.Success): void {
    this._notify.create(title, message, type);
  }

  public simpleConfirm(operation: Opr, entity: Ent, next: () => void): void {
    forkJoin([
      this._translate.get(`operation.${operation}`),
      this._translate.get(`entity.${entity}`),
      this._translate.get(`confirm.message`)
    ]).subscribe(([{ title }, entity, text]): void => {
      this.confirm({ title: `${title} ${entity}`, text })
        .then(({ value }: SweetAlertResult): void => !!value && next());
    });
  }

  public confirm(options: AlertOptions): Promise<void | SweetAlertResult> {
    return this.alert({
      icon: "warning",
      showCancelButton: true,
      cancelButtonText: this._translate.currentLang == 'fa' ? "خیر" : "No",
      cancelButtonClass: "btn btn-danger",
      ...options
    });
  }

  public alert(options: AlertOptions): Promise<void | SweetAlertResult> {
    return Swal.fire({
      icon: "success",
      confirmButtonText: this._translate.currentLang == 'fa' ? "بله" : "Yes",
      confirmButtonClass: "btn btn-primary",
      buttonsStyling: false,
      heightAuto: false,
      ...options
    }).then((res): SweetAlertResult => {
      return res;
    });
  }
}
