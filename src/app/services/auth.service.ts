import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private _http: HttpClient
  ) { }

  public login(user: { input: string, password: string }) {
    return this._http.post('user/login', user);
  }

  public getAuthToken():string {
    return localStorage.getItem('token')
  }

  public setAuthToken(token): void {
    localStorage.setItem('token', token);
  }
}
