import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { LocalStorageService } from 'angular-2-local-storage';

import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private _http: HttpClient,
    private _router: Router,
    private _storage: LocalStorageService
  ) { }

  public user(): Partial<User> {
    if(localStorage.getItem('current-user')) {
      return JSON.parse(localStorage.getItem('current-user')).user
    }
    else {
       return {
        user: {
          username: "Guest",
          fullname: "Guest",
          email: "",
          is_admin: false
        }
      } as Partial<User>
    }
  };

  public checkCookie() {
    var x = document.cookie;
    if (!x)
      this.logout();
  }

  public isAdmin(): boolean {
    return this.user().is_admin;
  }

  public getUsername(): string {
    return this.user().fullname || this.user().email;
  }

  public isLoggedIn() {
    if(localStorage.getItem('current-user')) return true;
    else return false;
  }

  public logout() {
    localStorage.removeItem('current-user');
    this._router.navigate(['/login']);
  }

  public ejectUser() {
    localStorage.removeItem('current-user');
    this._router.navigate(['/login']);
  }

  public getUsers() {
    return this._http.get('user/admin/list');
  }

  public getCurrentUser() {
    this._http.get('user/current').subscribe(res=> {
      this.setUserStorage(res);
    },
    err => {
      this.ejectUser();
    });
  }

  public setUserStorage(userStorage): void {
    delete userStorage.msg;
    delete userStorage.redirect_url;
    delete userStorage.stores;
    this._storage.set("currentUser", userStorage);
  }

  public getUserFromStorage() {
    if (!localStorage.getItem("user")) return;
    return JSON.parse(localStorage.getItem("user"));
  }

  public setUser(user) {
    return this._http.post('user/save', user);
  }

  public setUserAdmin(userAdmin) {
    return this._http.post('user/admin/add', userAdmin);
  }

  public editUser(id: string, user: User) {
    return this._http.put(`user/${id}`, user);
  }

  public deleteUser(id: string) {
    return this._http.delete(`user/${id}`);
  }

  public changeUserStatus(id: string, body) {
    return this._http.put(`user/admin/status`, body, { params: {id} });
  }
}
