import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Plans } from '../interfaces/plan';
@Injectable({
  providedIn: 'root'
})
export class PlanService {

  constructor(
    private _http: HttpClient
  ) { }

  public getPlans() {
    return this._http.get('plan/list');
  }

  public setPlan(plan) {
    return this._http.post('plan/save', plan);
  }

  public editPlan(id: string, plan: Plans) {
    return this._http.put(`plan/${id}`, plan);
  }

  public deletePlan(id: string) {
    return this._http.delete(`plan/${id}`);
  }

  // public togglePublish(id: string) {
  //   return this._http.put(``, {}, { params: {id} });
  // }
}
