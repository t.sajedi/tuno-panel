import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'makelist'
})
export class SplitPipe implements PipeTransform {

  transform(list: any, keyName: string, splitter: string = ', '): any {
    if (!list.length) return;
    if (list.length == 1) {return list[0][keyName]};
    let newList = [];
    for (let key in list) {
      if(list[key][keyName]) {
        newList.push(list[key][keyName]);
      }
    }
    return newList.join(splitter);
  }

}
