import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CKEditorModule } from "ckeditor4-angular";

import { CreateBlogComponent } from './create-blog/create-blog.component';
import { BlogListComponent } from './blog-list/blog-list.component';

@NgModule({
  declarations: [
    CreateBlogComponent,
    BlogListComponent
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    SharedModule,
    CKEditorModule
  ]
})
export class BlogModule { }
