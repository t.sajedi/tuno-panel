import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';

import { QueryService } from '../../../services/query.service'
import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styles: [
  ]
})
export class CreateBlogComponent implements OnInit {

  public blogID = this._route.snapshot.params['id'];
  public title: string = (this.blogID) ? 'blog.create.edit' : 'blog.create.title';

  constructor(
    private _httpService: HttpService,
    private _notify: NotifyService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    public query: QueryService
  ) {
    this._migrateForm();
  }

  ngOnInit(): void {
    this.getCategories();
  }

  public category = [];
  public defaultCategory = [];
  public getCategories() {
    this._httpService.invoke({
      method: 'GET',
      path: "category/list",
      query: { blog: true, limit: 20 }
    }).subscribe(
      res => {
        this.category = res.categoryes || [];
        if (this.blogID) this.getBlogInfo();
      },
      (err) => {
        if (this.blogID) this.getBlogInfo();
      }
    )
  }

  public form: FormGroup;
  private _migrateForm(): void {
    this.form = this._fb.group({
      "title": "",
      "image": ["", Validators.required],
      "category": "",
      "content": [""],
      "key": ["", Validators.required],
      "release": false
    });
  }
  
  public videoToCreate: string = "";
  public setCategory(event) {
    let id: string = "";
    for (const item of event) {
      id = item.id;
    }
    this.videoToCreate = id;
    this.form.get('category').patchValue(this.videoToCreate);
  }
    
  public getBlogInfo() {
    this._httpService.invoke({
      method: 'GET',
      path: `blog/load/${this.blogID}`
    }).subscribe(
      res => {
        this.form.controls['title'].patchValue(res.blog.title);
        this.form.controls['content'].patchValue(res.blog.content);
        this.form.controls['category'].patchValue(res.blog.category);
        this.form.controls['image'].patchValue(res.blog.image);
        this.form.patchValue(res.blog);
        this._makeDefaultImage(res.blog.image);
        this.defaultCategory = this.category.filter(category => category.id == res.blog.category);
      },
      (err) => {
      }
    )     
  }
  
  public setImage(images: string[], prefix: string): void {
    if(!images.length) {
      this.form.controls[prefix].patchValue("");
    }
    else {
      this.form.controls[prefix].patchValue(`/media/${images[0]}`);
    }
  }

  public defaultImage: any = [];
  private _makeDefaultImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultImage = images;
  }
  
  public formSubmited: boolean = false;
  public submitUrl: string = this.blogID ? `blog/update/${this.blogID}` : "blog/add";
  public submit(form) {
    this.formSubmited = true;
    if (form.invalid) return;
    this._httpService.invoke({
      method: this.blogID ? "PUT" : "POST",
      path: this.submitUrl,
      body: form.value
    }).subscribe(
      res => {
        this._notify.status(this.blogID ? Opr.Edit : Opr.Create, Ent.Blog);
        this.formSubmited = false;
        this._router.navigate(['/panel/blogs'], { queryParams: this.query.params() });
      },
      (err) => {
        console.log(err);
        this.formSubmited = false;
      }
    );
  }

  ngOnDestroy() {
  }
}