import { Component, OnDestroy, OnInit } from '@angular/core';

import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { QueryService } from '../../../services/query.service';
import { SearchService } from '../../../services/search.service';
import { HttpService } from 'src/app/services/http.service';

interface Filters {
  page: number,
  limit: number,
  sort: string,
  category: string;
  search?: string
}

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styles: [
  ]
})

export class BlogListComponent implements OnInit {

  constructor(
    private _httpService: HttpService,
    private _notify: NotifyService,
    public query: QueryService,
    public search: SearchService
  ) {
    this.getCategories();
    search.set("blog", (terms: string): void => {
      this.filters.search = terms;
      this.getBlog(1);
    });
  }

  ngOnInit(): void {}

  public filters: Partial<Filters> = {
    limit: 10,
    page: 1,
    sort: 'title',
    ...this.query.params()
  }

  public blogs = [];
  public totalPages: number = 1;
  public getBlog(pageNumber: number = this.filters.page) {
    this.filters.page = pageNumber;
    this._httpService.invoke({
      method: 'GET',
      path: 'blog/list',
      query: { ...this.filters }
    }).subscribe(
      res => {
        this.blogs = res.bloges || [];
        this.query.set(this.filters);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public deleteBlog(id: string) {
    this._notify.simpleConfirm(Opr.Del, Ent.Blog, () => {
      this._httpService.invoke({
        method: 'DELETE',
        path: `blog/remove/${id}`,
      }).subscribe(
        res => {
          this.getBlog();
        this._notify.status(Opr.Del, Ent.Blog);
        },
        (err) => {
          console.log(err)
        }
      )
    });
  };
  
  public sort(key: string): void {
    this.filters.sort = this.filters.sort == key ? `-${key}` : key;
    if (!this.blogs) { return; }
    this.getBlog(1);
  }
  public order(key: string): string[] {
    return ['sort', this.filters.sort == key ? 'order' :
      this.filters.sort == `-${key}` ? 'reverse' : ''];
  }

  public setFilter(filter?: string, value?: any): void {
    this.filters[filter] = value || (!this.filters[filter]);
    this.getBlog(1);
  }

  public videoToCreate: string = "";
  public setCategory(event) {
    let id: string = "";
    for (const item of event) {
      id = item.id;
    }
    this.videoToCreate = id;
    this.filters.category = this.videoToCreate;
    this.getBlog();
  }

  public publishBlog(_id) {
    this._httpService.invoke({
      method: 'GET',
      path: "blog/toggle/release/"+_id
    }).subscribe(
      res => {
        this.getBlog();
      },
      (err) => {
        console.log(err)
      }
    )
  }

  public category = [];
  public defaultCategory = [];
  public getCategories() {
    this._httpService.invoke({
      method: 'GET',
      path: "category/list",
      query: { blog: true, limit: 20 }
    }).subscribe(
      res => {
        this.category = res.categoryes || [];
        this.getBlog();
      },
      (err) => {
        console.log(err)
      }
    )
  }
  
  ngOnDestroy(): void {
  }

}
