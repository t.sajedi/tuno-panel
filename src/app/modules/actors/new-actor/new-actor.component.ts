import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { QueryService } from '../../../services/query.service';
import { UserService } from '../../../services/user.service';
import { HttpService } from 'src/app/services/http.service';
import Choices from 'choices.js';
interface TagFilters {
  page: number,
  limit: number,
  search: string;
  sort: string;
}

@Component({
  selector: 'app-new-actor',
  templateUrl: './new-actor.component.html',
  styles: [
  ]
})
export class NewActorComponent implements OnInit {
  public actorID = this._route.snapshot.params['id'];
  public title: string = (this.actorID) ? 'actors.create.edit' : 'actors.create.title';

  constructor(
    private _httpService: HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    public query: QueryService,
    public userService: UserService
  ) {
    this._migrateForm();
    if (this.actorID) this.getActorInfo();
  }

  ngOnInit(): void {
    this.getTags();
  }

  public form: FormGroup;
  private _migrateForm(): void {
    this.form = this._fb.group({
      "name": ["", Validators.required],
      "tag": [],
      "profile": "",
      "description": "",
      "key": ["", Validators.required]
    });
  }

  public setImage(images: string[], prefix: string): void {
    if(!images.length) {
      this.form.controls[prefix].patchValue("");
    }
    else {
      this.form.controls[prefix].patchValue(`/media/${images[0]}`);
    }
  }

  public defaultImage: any = [];
  private _makeDefaultImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultImage = images;
  }

  public remainingCharcters: number = 300;
  public countCharacters: number;
  public countingCharacters(event) {
    this.countCharacters = event.target.value.length;
    this.remainingCharcters = 300 - this.countCharacters;
  }

  getActorInfoApi;
  public getActorInfo() {
    this.getActorInfoApi = this._httpService.invoke({
      method: 'GET',
      path: `actor/load/${this.actorID}`
    }).subscribe(
      res => {
        this.countCharacters = res.actor.description.length;
        this.remainingCharcters = 300 - this.countCharacters;
        this.form.controls['name'].patchValue(res.actor.name);
        this.form.controls['description'].patchValue(res.actor.description);
        this.form.controls['profile'].patchValue(res.actor.profile);
        this.selectedTags = res.actor.tag || [];
        this.form.patchValue(res.actor);
        this._makeDefaultImage(res.actor.profile);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  // tags
  public tags = [];
  public selectedTags: any = [];
  getTagsApi;
  public getTags() {
    this.getTagsApi = this._httpService.invoke({
      method: 'GET',
      path: 'tag/list',
      query: { ...this.tagFilters }
    }).subscribe(
      res => {
        this.tags = res && (res.tages || []);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public tagFilters: Partial<TagFilters> = {
    limit: 30,
    page: 1,
    search: "",
    sort: "created-at"
  }

  public searchTag(searchValue) {
    this.tagFilters.search = this.converToEnDigit(searchValue);
    this.getTags();
  }

  public setTags(event: [{ name: string, id: string }]) {
    this.form.get("tag").patchValue(event);
  }

  public converToEnDigit(str) {
    return str
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/٠/g, "0")
      .replace(/١/g, "1")
      .replace(/٢/g, "2")
      .replace(/٣/g, "3")
      .replace(/٤/g, "4")
      .replace(/٥/g, "5")
      .replace(/٦/g, "6")
      .replace(/٧/g, "7")
      .replace(/٨/g, "8")
      .replace(/٩/g, "9");
  };

  public formSubmited: boolean = false;
  public submit(form: FormGroup) {
    this.formSubmited = true;
    if (form.invalid) return;
    var submitUrl = this.actorID ? "actor/update/" + this.actorID : "actor/add";
    this.getTagsApi = this._httpService.invoke({
      method: this.actorID ? "PUT" : "POST",
      path: submitUrl,
      body: form.value
    }).subscribe(
      res => {
        this.formSubmited = false;
        this._router.navigate(['/panel/actors'], { queryParams: this.query.params() });
      },
      (err) => {
        console.log(err);
        this.formSubmited = false;
      }
    );
  }

  ngOnDestroy() {
    if (this.getActorInfoApi) {
      this.getActorInfoApi.unsubscribe();
    }
  }
}
