import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActorsRoutingModule } from './actors-routing.module';
import { SharedModule } from '../shared/shared.module';

import { ActorListComponent } from './actor-list/actor-list.component';
import { NewActorComponent } from './new-actor/new-actor.component';

@NgModule({
  declarations: [
    ActorListComponent,
    NewActorComponent
  ],
  imports: [
    CommonModule,
    ActorsRoutingModule,
    SharedModule
  ]
})
export class ActorsModule { }
