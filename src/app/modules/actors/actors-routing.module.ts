import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ActorListComponent } from './actor-list/actor-list.component';
import { NewActorComponent } from './new-actor/new-actor.component';

const routes: Routes = [
  {
    path: 'list',
    component: ActorListComponent
  },
  {
    path: 'add',
    component: NewActorComponent
  },
  {
    path: 'edit/:id',
    component: NewActorComponent
  },
  {
    path: '',
    component: ActorListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActorsRoutingModule { }
