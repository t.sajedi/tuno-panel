import { Component, OnInit, OnDestroy } from '@angular/core';

import { TranslateService } from "@ngx-translate/core";

import { QueryService } from '../../../services/query.service';
import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { SearchService } from '../../../services/search.service';
import { Actors } from '../../../interfaces/actor';
import { UserService } from '../../../services/user.service';
import { HttpService } from '../../../services/http.service';

interface Filters {
  page: number,
  limit: number,
  sort: string,
  search?: string
}

@Component({
  selector: 'app-actor-list',
  templateUrl: './actor-list.component.html',
  styles: [
  ]
})
export class ActorListComponent implements OnInit, OnDestroy {

  constructor(
    private _notify: NotifyService,
    private _httpService: HttpService,
    public _translate: TranslateService,
    public query: QueryService,
    public userService: UserService,
    public search: SearchService
  ) {
    this.getActors();
    search.set("actors", (terms: string): void => {
      this.filters.search = terms;
      this.getActors(1);
    });
  }

  ngOnInit() { }

  public filters: Partial<Filters> = {
    limit: 12,
    page: 1,
    sort: '-created_at',
    ...this.query.params()
  }

  public actors: Actors[] = [];
  public totalPages: number = 1;
  getActor;
  public getActors(pageNumber: number = this.filters.page) {
    console.log("hkjh", pageNumber)
    this.filters.page = pageNumber;
    this.rowNumber();
 
    this.getActor = this._httpService.invoke({
      method: 'GET',
      path: 'actor/list',
      query: { ...this.filters }
    }).subscribe(
      res => {
        this.actors = res.actores || [];
        this.totalPages = res.totalPage;
        this.filters.page = res.page;
        this.query.set(this.filters);
      },
      (err) => {
        console.log(err)
      }
    )
  }

  public rowOffset: number = 0;
  private rowNumber() {
    this.rowOffset = (this.filters.page == 1 ? 0 : (this.filters.page - 1) * this.filters.limit) + 1;
  }

  deleteActorApi;
  public deleteActor(id: string) {
    this._notify.simpleConfirm(Opr.Del, Ent.Actor, () => {
      this.deleteActorApi = this._httpService.invoke({
        method: 'DELETE',
        path: `actor/remove/${id}`,
      }).subscribe(
        res => {
          this.getActors(1);
          this._notify.status(Opr.Del, Ent.Actor);
        },
        (err) => {
          console.log(err)
        }
      )
    });
  }

  public sort(key: string): void {
    this.filters.sort = this.filters.sort == key ? `-${key}` : key;
    if (!this.actors) { return; }
    this.getActors(1);
  }
  public order(key: string): string[] {
    return ['sort', this.filters.sort == key ? 'order' :
      this.filters.sort == `-${key}` ? 'reverse' : ''];
  }

  ngOnDestroy() {
    if (this.getActor) {
      this.getActor.unsubscribe();
    }
    if (this.deleteActorApi) {
      this.deleteActorApi.unsubscribe();
    }
    this.search.remove('actors');
  }

}