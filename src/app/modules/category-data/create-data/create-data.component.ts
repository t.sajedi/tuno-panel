import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

import { QueryService } from '../../../services/query.service';
import { UserService } from '../../../services/user.service';
import { HttpService } from 'src/app/services/http.service';

interface VideoFilters {
  page: number,
  limit: number,
  search: string;
  sort: string;
}

@Component({
  selector: 'app-create-data',
  templateUrl: './create-data.component.html',
  styles: [
  ]
})
export class CreateDataComponent implements OnInit {
  public categoryID = this._route.snapshot.params['id'];
  public dataID = this._route.snapshot.params['dataID'];
  public title: string = (this.dataID) ? 'category.data.edit' : 'category.data.create';

  constructor(
    private _httpService: HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    public query: QueryService,
    public userService: UserService,
  ) {
    this._migrateForm();
    this.getVideos();
  }

  ngOnInit(): void {}

  public form: FormGroup;
  private _migrateForm(): void {
    this.form = this._fb.group({
      "name": ["", Validators.required],
      "profile": "",
      "category": [this.categoryID, Validators.required],
      "posters": [[]],
      "description": "",
      "type": 1,
      "key": ["", Validators.required],
      "season": this._fb.array([])
    });
  }

  // season
  public seasonVideos(): FormArray {
    return this.form.get('season') as FormArray;
  }

  public removeVideoLink(vIndex: number) {
    this.seasonVideos().removeAt(vIndex);
  }

  public addVideo(i?: number, videoUrls?:any) {
    this.seasonVideos().push(this.newVideo(videoUrls, i));
  }
  public newVideo(videoUrls?:any, formArrayIndex?: any): FormGroup {
    let defaultVideo = [];
    if(videoUrls) {
      for(let i = 0; i < videoUrls.videos.length; i++) {
        defaultVideo.push({"id": videoUrls.videos[i], "name": (this.videos.find(video => video.id == videoUrls.videos[i]) ||[]).name })
      }
    }
    else {
      defaultVideo = [];
    }
    this.selectedVideo[formArrayIndex] = defaultVideo;
    return this._fb.group({
      name: [videoUrls ? videoUrls.name : '', Validators.required],
      description: [videoUrls ? videoUrls.description : ''],
      videos: [videoUrls ? videoUrls.videos : []]
    });
  }

  // set season video
    // related films
  public videos = [];
  public selectedVideo: any = [];
  public getVideos() {
    this._httpService.invoke({
      method: 'GET',
      path: 'video/list',
      query: { ...this.videoFilters }
    }).subscribe(
      res => {
        this.videos = res && (res.videos || []);
        if (this.dataID) {
          this.getDataInfo();
        }
      },
      (err) => {
        if (this.dataID) {
          this.getDataInfo();
        }
      }
    );
  }

  public videoFilters: Partial<VideoFilters> = {
    limit: 20,
    page: 1,
    search: "",
    sort: "-created-at"
  }

  public searchVideo(searchValue) {
    this.videoFilters.search = this.converToEnDigit(searchValue);
    this.getVideos();
  }

  public videoToCreate: any = [];
  public setVideo(event: [{ name: string, id: string }], formArrayIndex) {
    const idArray: string[] = [];
    for (const item of event) {
      idArray.push(item.id)
    }
    this.videoToCreate = Array.from(new Set(idArray));
    this.seasonVideos()
      .at(formArrayIndex)
      .get('videos').patchValue(this.videoToCreate);
  }

  public converToEnDigit(str) {
    return str
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/٠/g, "0")
      .replace(/١/g, "1")
      .replace(/٢/g, "2")
      .replace(/٣/g, "3")
      .replace(/٤/g, "4")
      .replace(/٥/g, "5")
      .replace(/٦/g, "6")
      .replace(/٧/g, "7")
      .replace(/٨/g, "8")
      .replace(/٩/g, "9");
  };

  public setImage(images: string[], prefix: string): void {
    if(!images.length) {
      this.form.controls[prefix].patchValue("");
    }
    else {
      this.form.controls[prefix].patchValue(`/media/${images[0]}`);
    }
  }

  public defaultImage: any = [];
  private _makeDefaultImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultImage = images;
  }

  // posters
  public defaultPosters: any = [];
  private _makeDefaultPosters(video): void {
    let images = [];
    if (video.posters) {
      video.posters.forEach((original: string, i: number): void => {
        let name: string[] = original.split("/");
        images.push({
          original_name: name[name.length - 1],
          path: original,
          thumb: "",
          micro: "",
          big: "",
          mid: ""
        });
      });
    }
    this.defaultPosters = images;
  }

  public setPosters(images: string[]): void {
    this.form.controls['posters'].patchValue(images);
  }

  public remainingCharcters: number = 300;
  public countCharacters: number;
  public countingCharacters(event) {
    this.countCharacters = event.target.value.length;
    this.remainingCharcters = 300 - this.countCharacters;
  }

  public getDataInfo() {
    this._httpService.invoke({
      method: 'GET',
      path: `category/data/load/${this.dataID}`
    }).subscribe(
      res => {
        this.countCharacters = res.data.description.length;
        this.remainingCharcters = 300 - this.countCharacters;
        this.form.controls['name'].patchValue(res.data.name);
        this.form.controls['description'].patchValue(res.data.description);
        this.form.controls['profile'].patchValue(res.data.profile);
        this.form.controls['posters'].patchValue(res.data.posters);
        this.form.patchValue(res.data);
        this._makeDefaultImage(res.data.profile);
        this._makeDefaultPosters(res.data);
        // patch season
        for(let i = 0; i < res.data.season.length; i++) {
          this.addVideo(i, res.data.season[i]);
        }
      },
      (err) => {
      }
    );
  }

  public formSubmited: boolean = false;
  public submit(form: FormGroup) {
    this.formSubmited = true;
    if (form.invalid) return;
    var submitUrl = this.dataID ? "category/data/update/" + this.dataID : "category/data/add";
    this._httpService.invoke({
      method: this.dataID ? "PUT" : "POST",
      path: submitUrl,
      body: form.value
    }).subscribe(
      res => {
        this.formSubmited = false;
        this._router.navigate(['/panel/data/list/' + this.categoryID], { queryParams: this.query.params() });
      },
      (err) => {
        this.formSubmited = false;
      }
    );
  }

  ngOnDestroy() {
  }
}
