import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TranslateService } from "@ngx-translate/core";

import { QueryService } from '../../../services/query.service';
import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { SearchService } from '../../../services/search.service';
import { UserService } from '../../../services/user.service';
import { CategoryData } from '../../../interfaces/category-data';
import { HttpService } from 'src/app/services/http.service';

interface getData {
  dataes: CategoryData[];
  totalPage: number;
  page: number;
}
interface Filters {
  page: number;
  limit: number;
  sort: string;
  search?: string;
  type?: number;
  category: string;
}

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styles: [
  ]
})
export class DataListComponent implements OnInit, OnDestroy {

  public categoryID = this._route.snapshot.params['id'];
  public categoryName = "";
  constructor(
    private _route: ActivatedRoute,
    private _notify: NotifyService,
    private _httpService: HttpService,
    public _translate: TranslateService,
    public query: QueryService,
    public userService: UserService,
    public search: SearchService
  ) {
    this.getData();
    search.set("data", (terms: string): void => {
      this.filters.search = terms;
      this.getData(1);
    });
  }

  ngOnInit() {
    this.getCategoryName();
  }

  public filters: Partial<Filters> = {
    limit: 12,
    page: 1,
    sort: '-created_at',
    type: 1,
    category: this.categoryID,
    ...this.query.params()
  }

  public data: CategoryData[] = [];
  public totalPages: number = 1;
  public getData(pageNumber: number = this.filters.page) {
    this.filters.page = pageNumber;
    this.rowNumber();
    this._httpService.invoke({
      method: 'GET',
      path: 'category/data/list',
      query: { ...this.filters }
    }).subscribe(
      res => {
        this.data = res.dataes || [];
        this.totalPages = res.totalPage;
        this.filters.page = res.page;
        this.query.set(this.filters);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public rowOffset: number = 0;
  private rowNumber() {
    this.rowOffset = (this.filters.page == 1 ? 0 : (this.filters.page - 1) * this.filters.limit) + 1;
  }

  public getCategoryName() {
    this._httpService.invoke({
      method: 'GET',
      path: 'category/load/' + this.categoryID,
      query: { ...this.filters }
    }).subscribe(
      res => {
        let data = res.category || [];
        this.categoryName = data.name;
        this.totalPages = res.totalPage;
        this.filters.page = res.page;
        this.query.set(this.filters);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public deleteData(id: string) {
    this._notify.simpleConfirm(Opr.Del, Ent.CategoryData, () => {
      this._httpService.invoke({
        method: 'DELETE',
        path: `category/data/remove/${id}`,
      }).subscribe(
        res => {
          this.getData(1);
          this._notify.status(Opr.Del, Ent.CategoryData);
        },
        (err) => {
          console.log(err)
        }
      )
    });
  }

  public sort(key: string): void {
    this.filters.sort = this.filters.sort == key ? `-${key}` : key;
    if (!this.data) { return; }
    this.getData(1);
  }
  public order(key: string): string[] {
    return ['sort', this.filters.sort == key ? 'order' :
      this.filters.sort == `-${key}` ? 'reverse' : ''];
  }

  public setFilter(filter?: string, value?: any): void {
    this.filters[filter] = value || (this.filters[filter] == 1 ? this.filters[filter] = 2 : this.filters[filter] = 1);
    this.getData(1);
  }

  ngOnDestroy() {
    this.search.remove('categories');
  }

}