import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryDataRoutingModule } from './category-data-routing.module';
import { SharedModule } from '../shared/shared.module';

import { CreateDataComponent } from './create-data/create-data.component';
import { DataListComponent } from './data-list/data-list.component';


@NgModule({
  declarations: [
    CreateDataComponent,
    DataListComponent
  ],
  imports: [
    CommonModule,
    CategoryDataRoutingModule,
    SharedModule
  ]
})
export class CategoryDataModule { }
