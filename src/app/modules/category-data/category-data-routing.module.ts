import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateDataComponent } from './create-data/create-data.component';
import { DataListComponent } from './data-list/data-list.component';

const routes: Routes = [
  {
    path: 'list/:id',
    component: DataListComponent
  },
  {
    path: 'add/:id',
    component: CreateDataComponent
  },
  {
    path: 'edit/:id/:dataID',
    component: CreateDataComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryDataRoutingModule { }
