import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlansRoutingModule } from './plans-routing.module';
import { SharedModule } from "../shared/shared.module";

import { PlansListComponent } from './plans-list/plans-list.component';
import { NewPlanComponent } from './new-plan/new-plan.component';

@NgModule({
  declarations: [
    PlansListComponent,
    NewPlanComponent
  ],
  imports: [
    CommonModule,
    PlansRoutingModule,
    SharedModule
  ]
})
export class PlansModule { }
