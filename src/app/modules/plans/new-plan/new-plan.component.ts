import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { QueryService } from '../../../services/query.service';
import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { StorageService } from '../../../services/storage.service';

@Component({
  selector: 'app-new-plan',
  templateUrl: './new-plan.component.html',
  styles: [
  ]
})
export class NewPlanComponent implements OnInit {

  public planID = this._route.snapshot.params['id'];
  public title: string = (this.planID) ? 'subscription.create.edit' : 'subscription.create.title';
  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _route: ActivatedRoute,
    private _notify: NotifyService,
    private _storage: StorageService,
    public query: QueryService
  ) {
    if (this.planID) this._getPlan();
  }

  ngOnInit(): void {}

  public form = this._fb.group({
    "subscription_name_fa": ["", Validators.required],
    "subscription_name_en": "",
    "subscription_price": [0, Validators.required],
    "subscription_duration": [0, Validators.required],
    "discount": [0]
  });

  private _getPlan(): void {
    let plan: any = this._storage.get(this.planID);
    if (plan) {
      this._storage.remove(this.planID);
      this.form.patchValue(plan);
      this.form.get("discount").patchValue(plan.discount.split("%")[0]);
      this.form.get("subscription_duration").patchValue(plan.subscription_duration);
    } else
      this._router.navigate(['/subscription'], { queryParams: this.query.params() });
  }

  public toEnglish(price) {
    let persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
      arabicNumbers = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g];
    if (typeof price === 'string') {
      for (let i = 0; i < 10; i++) {
        price = price.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
      }
    }
    return parseInt(price);
  }

  public toDuration(duration) {
    let hours = Number(duration);
    if (Math.sign(hours) < 0) {
      hours = Math.abs(hours);
    }

    const days = Math.floor(hours / 24);
    const months = Math.floor(hours / (24 * 30));
    const years = Math.floor(hours / (24 * 30 * 12));
    let d = days > 0 && days;
    let m = months > 0 && months;
    let y = years > 0 && years;
    this.duration_type = years > 0 ? 1 : months > 0 ? 2 : 3;
    return years > 0 ? y : months > 0 ? m : d;
  }

  public duration_type = 1;
  public formSubmited: boolean = false;
  public submit(form) {
    this.formSubmited = true;
    if (form.invalid) return;
  }

  ngOnDestroy() {
  }

}
