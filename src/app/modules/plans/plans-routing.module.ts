import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PlansListComponent } from './plans-list/plans-list.component';
import { NewPlanComponent } from './new-plan/new-plan.component';

const routes: Routes = [
  {
    path: 'list',
    component: PlansListComponent
  },
  {
    path: 'add',
    component: NewPlanComponent
  },
  {
    path: 'edit/:id',
    component: NewPlanComponent
  },
  {
    path: '',
    component: PlansListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlansRoutingModule { }
