import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { QueryService } from '../../../services/query.service';
import { StorageService } from '../../../services/storage.service';

interface Filters {
  page: number,
  limit: number
}

@Component({
  selector: 'app-plans-list',
  templateUrl: './plans-list.component.html',
  styles: [
  ]
})
export class PlansListComponent implements OnInit, OnDestroy {

  public p: number;
  constructor(
    private _router: Router,
    public query: QueryService,
    private _notify: NotifyService,
    private _storage: StorageService
  ) { }

  ngOnInit() {
  }

  public filters: Partial<Filters> = {
    limit: 10,
    page: 1,
    ...this.query.params()
  }

  public subscriptions = [];
  public getSubscriptions() {
  
  }

  public deleteSubscription(id: string) {
    this._notify.simpleConfirm(Opr.Del, Ent.Plan, () => {
  
    });
  }

  public publishSubscription(event, _id) {
   
  }

  public passToEdit(plan: any): void {
    this._storage.set(plan.id, plan);
    this._router.navigate(['/subscription/edit', plan.id], { queryParams: this.query.params() });
  }

  // public u: Plans;
  // public plans = [];
  // public showSpinner: boolean = true;
  // public showPlans(): void {
  //   this.getU = this._planService.getPlans().subscribe((res: {plans: Plans}) => {
  //     this.showSpinner = false;
  //     this.u = { ...res.plans };
  //     this.p = 1;
  //     this.plans = [];
  //     for(const key in this.u)
  //       this.plans.push(this.u[key]);
  //     }, err => {
  //       this.showSpinner = false;
  //       console.log(err);
  //     }
  //   );
  // }

  public order: string = 'username';
  public reverse: boolean = false;
  public setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }

  ngOnDestroy() {
  }

}
