import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { QueryService } from '../../../services/query.service';
import { UserService } from '../../../services/user.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-create-tag',
  templateUrl: './create-tag.component.html',
  styles: [
  ]
})
export class CreateTagComponent implements OnInit {
  public tagID = this._route.snapshot.params['id'];
  public title: string = (this.tagID) ? 'tag.create.edit' : 'tag.create.title';

  constructor(
    private _httpService: HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    public query: QueryService,
    public userService: UserService,
  ) {
    this._migrateForm();
    if (this.tagID) this.getTag();
  }

  ngOnInit(): void {}

  public form: FormGroup;
  private _migrateForm(): void {
    this.form = this._fb.group({
      "name": ["", Validators.required],
      "profile": "",
      "description": "",
      "key": ["", Validators.required]
    });
  }

  public remainingCharcters: number = 300;
  public countCharacters: number;
  public countingCharacters(event) {
    this.countCharacters = event.target.value.length;
    this.remainingCharcters = 300 - this.countCharacters;
  }

  public profile: string = "";
  public getTag() {
    this._httpService.invoke({
      method: 'GET',
      path: `tag/load/${this.tagID}`
    }).subscribe(
      res => {
        this.countCharacters = res.tag.description.length;
        this.remainingCharcters = 300 - this.countCharacters;
        this.form.controls['name'].patchValue(res.tag.name);
        this.form.controls['profile'].patchValue(res.tag.profile);
        this.form.controls['description'].patchValue(res.tag.description);
        this._makeDefaultImage(res.tag.profile);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public setImage(images: string[], prefix: string): void {
    if(!images.length) {
      this.form.controls[prefix].patchValue("");
    }
    else {
      this.form.controls[prefix].patchValue(`/media/${images[0]}`);
    }
  }

  public defaultImage: any = [];
  private _makeDefaultImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultImage = images;
  }

  public formSubmited: boolean = false;
  public submit(form: FormGroup) {
    this.formSubmited = true;
    if (form.invalid) return;
    var submitUrl = this.tagID ? "tag/update/" + this.tagID : "tag/add";
    this._httpService.invoke({
      method: this.tagID ? "PUT" : "POST",
      path: submitUrl,
      body: form.value
    }).subscribe(
      res => {
        this._router.navigate(['/panel/tag'], { queryParams: this.query.params() });
      },
      (err) => {
        console.log(err);
        this.formSubmited = false;
      }
    );
  }
}
