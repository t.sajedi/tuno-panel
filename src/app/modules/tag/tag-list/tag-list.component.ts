import { Component, OnInit } from '@angular/core';

import { QueryService } from '../../../services/query.service';
import { UserService } from '../../../services/user.service';
import { SearchService } from '../../../services/search.service';
import { Ent, NotifyService, Opr } from '../../../services/notify.service';

import { Tag } from '../../../interfaces/tag';
import { HttpService } from 'src/app/services/http.service';

interface getArtistsList {
  tages: Tag[];
  totalPage: number;
  page: number;
}

interface Filters {
  page: number,
  limit: number,
  sort: string,
  search?: string
}

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styles: [
  ]
})
export class TagListComponent implements OnInit {

  constructor(
    private _httpService: HttpService,
    private _notify: NotifyService,
    public query: QueryService,
    public userService: UserService,
    public search: SearchService
  ) { }

  ngOnInit(): void {
    this.getTags();
    this.search.set("tags", (terms: string): void => {
      this.filters.search = terms;
      this.getTags(1);
    });
  }

  public filters: Partial<Filters> = {
    limit: 10,
    page: 1,
    sort: 'name',
    ...this.query.params()
  }

  public tags: any[] = [];
  public totalPages: number = 1;
  public getTags(pageNumber: number = this.filters.page) {
    this.filters.page = pageNumber;
    this.rowNumber();
    this._httpService.invoke({
      method: 'GET',
      path: 'tag/list',
      query: { ...this.filters }
    }).subscribe(
      res => {
        this.tags = res.tages || [];
        this.totalPages = res.totalPage;
        this.filters.page = res.page;
        this.query.set(this.filters);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public rowOffset: number = 0;
  private rowNumber() {
    this.rowOffset = (this.filters.page == 1 ? 0 : (this.filters.page - 1) * this.filters.limit) + 1;
  }

  public deleteTag(id: string) {
    this._notify.simpleConfirm(Opr.Del, Ent.Tag, () => {
      this._httpService.invoke({
        method: 'DELETE',
        path: `tag/remove/${id}`,
      }).subscribe(
        res => {
          this.getTags(1);
          this._notify.status(Opr.Del, Ent.Tag);
        },
        (err) => {
          console.log(err)
        }
      )
    });
  }

  public sort(key: string): void {
    this.filters.sort = this.filters.sort == key ? `-${key}` : key;
    if (!this.tags) { return; }
    this.getTags(1);
  }
  public order(key: string): string[] {
    return ['sort', this.filters.sort == key ? 'order' :
      this.filters.sort == `-${key}` ? 'reverse' : ''];
  }

  ngOnDestroy() {
    this.search.remove("tags");
  }

}
