import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TagRoutingModule } from './tag-routing.module';
import { SharedModule } from '../shared/shared.module';

import { TagListComponent } from './tag-list/tag-list.component';
import { CreateTagComponent } from './create-tag/create-tag.component';


@NgModule({
  declarations: [
    TagListComponent,
    CreateTagComponent
  ],
  imports: [
    CommonModule,
    TagRoutingModule,
    SharedModule
  ]
})
export class TagModule { }
