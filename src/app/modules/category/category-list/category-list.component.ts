
import { Component, OnInit, OnDestroy } from '@angular/core';

import { TranslateService } from "@ngx-translate/core";

import { QueryService } from '../../../services/query.service';
import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { SearchService } from '../../../services/search.service';
import { UserService } from '../../../services/user.service';
import { Categories } from '../../../interfaces/category';
import { HttpService } from 'src/app/services/http.service';

interface Filters {
  page: number,
  limit: number,
  sort: string,
  blog: boolean,
  search?: string
}

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styles: [
  ]
})
export class CategoryListComponent implements OnInit {

  constructor(
    private _notify: NotifyService,
    private _httpService: HttpService,
    public _translate: TranslateService,
    public query: QueryService,
    public userService: UserService,
    public search: SearchService
  ) {
    this.getCategories();
    search.set("categories", (terms: string): void => {
      this.filters.search = terms;
      this.getCategories(1);
    });
  }

  ngOnInit() { }

  public filters: Partial<Filters> = {
    limit: 12,
    page: 1,
    sort: '-created_at',
    blog: false,
    ...this.query.params()
  }

  public categories: Categories[] = [];
  public totalPages: number = 1;
  public getCategories(pageNumber: number = this.filters.page) {
    this.filters.page = pageNumber;
    this.rowNumber();
    this._httpService.invoke({
      method: 'GET',
      path: 'category/list',
      query: { ...this.filters }
    }).subscribe(
      res => {
        this.categories = res.categoryes || [];
        this.totalPages = res.totalPage;
        this.filters.page = res.page;
        this.query.set(this.filters);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public rowOffset: number = 0;
  private rowNumber() {
    this.rowOffset = (this.filters.page == 1 ? 0 : (this.filters.page - 1) * this.filters.limit) + 1;
  }

  public deleteCategory(id: string) {
    this._notify.simpleConfirm(Opr.Del, Ent.Category, () => {
      this._httpService.invoke({
        method: 'DELETE',
        path: `category/remove/${id}`,
      }).subscribe(
        res => {
          this.getCategories(1);
          this._notify.status(Opr.Del, Ent.Category);
        },
        (err) => {
          console.log(err)
        }
      )
    });
  }

  public sort(key: string): void {
    this.filters.sort = this.filters.sort == key ? `-${key}` : key;
    if (!this.categories) { return; }
    this.getCategories(1);
  }
  public order(key: string): string[] {
    return ['sort', this.filters.sort == key ? 'order' :
      this.filters.sort == `-${key}` ? 'reverse' : ''];
  }

  public setFilter(filter?: string, value?: any): void {
    this.filters[filter] = value || (!this.filters[filter]);
    this.getCategories(1);
  }

  ngOnDestroy() {
    this.search.remove('categories');
  }

}