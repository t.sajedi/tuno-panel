import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { QueryService } from '../../../services/query.service';
import { UserService } from '../../../services/user.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styles: [
  ]
})
export class CreateCategoryComponent implements OnInit {
  public categoryID = this._route.snapshot.params['id'];
  public title: string = (this.categoryID) ? 'category.create.edit' : 'category.create.title';

  constructor(
    private _httpService: HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    public query: QueryService,
    public userService: UserService,
  ) {
    this._migrateForm();
    if (this.categoryID) this.getCategoryInfo();
  }

  ngOnInit(): void {}

  public form: FormGroup;
  private _migrateForm(): void {
    this.form = this._fb.group({
      "name": ["", Validators.required],
      "type": [1],
      "profile": "",
      "posters": [[]],
      "description": "",
      "blog": false,
      "key": ["", Validators.required]
    });
  }

  public setImage(images: string[], prefix: string): void {
    if(!images.length) {
      this.form.controls[prefix].patchValue("");
    }
    else {
      this.form.controls[prefix].patchValue(`/media/${images[0]}`);
    }
  }

  public defaultImage: any = [];
  private _makeDefaultImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultImage = images;
  }

  // posters
  public defaultPosters: any = [];
  private _makeDefaultPosters(video): void {
    let images = [];
    if (video.posters) {
      video.posters.forEach((original: string, i: number): void => {
        let name: string[] = original.split("/");
        images.push({
          original_name: name[name.length - 1],
          path: original,
          thumb: "",
          micro: "",
          big: "",
          mid: ""
        });
      });
    }
    this.defaultPosters = images;
  }

  public setPosters(images: string[]): void {
    this.form.controls['posters'].patchValue(images);
  }

  public remainingCharcters: number = 300;
  public countCharacters: number;
  public countingCharacters(event) {
    this.countCharacters = event.target.value.length;
    this.remainingCharcters = 300 - this.countCharacters;
  }

  public getCategoryInfo() {
    this._httpService.invoke({
      method: 'GET',
      path: `category/load/${this.categoryID}`,
    }).subscribe(
      res => {
        this.countCharacters = res.category.description.length;
        this.remainingCharcters = 300 - this.countCharacters;
        this.form.controls['name'].patchValue(res.category.name);
        this.form.controls['description'].patchValue(res.category.description);
        this.form.controls['profile'].patchValue(res.category.profile);
        this.form.controls['posters'].patchValue(res.category.posters);
        this.form.patchValue(res.category);
        this._makeDefaultImage(res.category.profile);
        this._makeDefaultPosters(res.category);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public formSubmited: boolean = false;
  public submit(form: FormGroup) {
    this.formSubmited = true;
    if (form.invalid) return;
    var submitUrl = this.categoryID ? "category/update/" + this.categoryID : "category/add";
    this._httpService.invoke({
      method: this.categoryID ? "PUT" : "POST",
      path: submitUrl,
      body: form.value
    }).subscribe(
      res => {
        this.formSubmited = false;
        this._router.navigate(['/panel/category'], { queryParams: this.query.params() });
      },
      (err) => {
        console.log(err);
        this.formSubmited = false;
      }
    );
  }

  ngOnDestroy() {
  }
}
