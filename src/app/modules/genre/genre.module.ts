import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GenreRoutingModule } from './genre-routing.module';
import { SharedModule } from '../shared/shared.module';

import { CreateGenreComponent } from './create-genre/create-genre.component';
import { GenreListComponent } from './genre-list/genre-list.component';

@NgModule({
  declarations: [
    CreateGenreComponent,
    GenreListComponent
  ],
  imports: [
    CommonModule,
    GenreRoutingModule,
    SharedModule
  ]
})
export class GenreModule { }
