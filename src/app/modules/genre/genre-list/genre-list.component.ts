import { Component, OnInit } from '@angular/core';

import { QueryService } from '../../../services/query.service';
import { UserService } from '../../../services/user.service';
import { SearchService } from '../../../services/search.service';
import { Ent, NotifyService, Opr } from '../../../services/notify.service';

import { HttpService } from 'src/app/services/http.service';

interface Filters {
  page: number,
  limit: number,
  sort: string,
  search?: string
}

@Component({
  selector: 'app-genre-list',
  templateUrl: './genre-list.component.html',
  styles: [
  ]
})
export class GenreListComponent implements OnInit {

  constructor(
    private _httpService: HttpService,
    private _notify: NotifyService,
    public query: QueryService,
    public userService: UserService,
    public search: SearchService
  ) { }

  ngOnInit(): void {
    this.getGenre();
    this.search.set("genrees", (terms: string): void => {
      this.filters.search = terms;
      this.getGenre(1);
    });
  }

  public filters: Partial<Filters> = {
    limit: 10,
    page: 1,
    sort: 'name',
    ...this.query.params()
  }

  public genrees: any[] = [];
  public totalPages: number = 1;
  public getGenre(pageNumber: number = this.filters.page) {
    this.filters.page = pageNumber;
    this.rowNumber();
    this._httpService.invoke({
      method: 'GET',
      path: 'genre/list',
      query: { ...this.filters }
    }).subscribe(
      res => {
        this.genrees = res.genrees || [];
        this.totalPages = res.totalPage;
        this.filters.page = res.page;
        this.query.set(this.filters);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public rowOffset: number = 0;
  private rowNumber() {
    this.rowOffset = (this.filters.page == 1 ? 0 : (this.filters.page - 1) * this.filters.limit) + 1;
  }

  public deleteGenre(id: string) {
    this._notify.simpleConfirm(Opr.Del, Ent.Genre, () => {
      this._httpService.invoke({
        method: 'DELETE',
        path: `genre/remove/${id}`,
      }).subscribe(
        res => {
          this.getGenre(1);
          this._notify.status(Opr.Del, Ent.Director);
        },
        (err) => {
          console.log(err)
        }
      )
    });
  }

  public sort(key: string): void {
    this.filters.sort = this.filters.sort == key ? `-${key}` : key;
    if (!this.genrees) { return; }
    this.getGenre(1);
  }
  public order(key: string): string[] {
    return ['sort', this.filters.sort == key ? 'order' :
      this.filters.sort == `-${key}` ? 'reverse' : ''];
  }

  ngOnDestroy() {
    this.search.remove("genrees");
  }

}
