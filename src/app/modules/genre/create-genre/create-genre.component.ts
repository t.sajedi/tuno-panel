import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { QueryService } from '../../../services/query.service';
import { UserService } from '../../../services/user.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-create-genre',
  templateUrl: './create-genre.component.html',
  styles: [
  ]
})
export class CreateGenreComponent implements OnInit {
  public genreID = this._route.snapshot.params['id'];
  public title: string = (this.genreID) ? 'genre.create.edit' : 'genre.create.title';

  constructor(
    private _httpService: HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    public query: QueryService,
    public userService: UserService,
  ) {
    this._migrateForm();
    if (this.genreID) this.getDirector();
  }

  ngOnInit(): void {}

  public form: FormGroup;
  private _migrateForm(): void {
    this.form = this._fb.group({
      "name": ["", Validators.required],
      "profile": "",
      "description": "",
      "key": ["", Validators.required]
    });
  }

  public remainingCharcters: number = 300;
  public countCharacters: number;
  public countingCharacters(event) {
    this.countCharacters = event.target.value.length;
    this.remainingCharcters = 300 - this.countCharacters;
  }

  public profile: string = "";
  public getDirector() {
    this._httpService.invoke({
      method: 'GET',
      path: `genre/load/${this.genreID}`
    }).subscribe(
      res => {
        this.countCharacters = res.genre.description.length;
        this.remainingCharcters = 300 - this.countCharacters;
        this.form.controls['name'].patchValue(res.genre.name);
        this.form.controls['profile'].patchValue(res.genre.profile);
        this.form.controls['description'].patchValue(res.genre.description);
        this._makeDefaultImage(res.genre.profile);
      },
      (err) => {
        console.log(err)
      }
    )
  }

  public setImage(images: string[], prefix: string): void {
    if(!images.length) {
      this.form.controls[prefix].patchValue("");
    }
    else {
      this.form.controls[prefix].patchValue(`/media/${images[0]}`);
    }
  }

  public defaultImage: any = [];
  private _makeDefaultImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultImage = images;
  }

  public formSubmited: boolean = false;
  public submit(form: FormGroup) {
    this.formSubmited = true;
    if (form.invalid) return;
    var submitUrl = this.genreID ? "genre/update/" + this.genreID : "genre/add";
    this._httpService.invoke({
      method: this.genreID ? "PUT" : "POST",
      path: submitUrl,
      body: form.value
    }).subscribe(
      res => {
        this._router.navigate(['/panel/genre'], { queryParams: this.query.params() });
      },
      (err) => {
        console.log(err);
        this.formSubmited = false;
      }
    );
  }
}
