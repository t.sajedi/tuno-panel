import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GenreListComponent } from './genre-list/genre-list.component';
import { CreateGenreComponent } from './create-genre/create-genre.component';

const routes: Routes = [
  {
    path: 'list',
    component: GenreListComponent
  },
  {
    path: 'add',
    component: CreateGenreComponent
  },
  {
    path: 'edit/:id',
    component: CreateGenreComponent
  },
  {
    path: '',
    component: GenreListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenreRoutingModule { }
