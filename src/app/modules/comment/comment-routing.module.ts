import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommentListComponent } from './comment-list/comment-list.component';
import { ViewCommentComponent } from './view-comment/view-comment.component';

const routes: Routes = [
  {
    path: 'list/:videoID',
    component: CommentListComponent
  },
  {
    path: ':commentID',
    component: ViewCommentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommentRoutingModule { }
