import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommentRoutingModule } from './comment-routing.module';
import { SharedModule } from '../shared/shared.module';

import { ViewCommentComponent } from './view-comment/view-comment.component';
import { CommentListComponent } from './comment-list/comment-list.component';


@NgModule({
  declarations: [
    ViewCommentComponent,
    CommentListComponent
  ],
  imports: [
    CommonModule,
    CommentRoutingModule,
    SharedModule
  ]
})
export class CommentModule { }
