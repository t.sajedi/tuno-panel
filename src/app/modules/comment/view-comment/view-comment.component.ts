import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { QueryService } from '../../../services/query.service';
import { StorageService } from '../../../services/storage.service';
import { HttpService } from 'src/app/services/http.service';

interface getVideo {
  video: any;
}
@Component({
  selector: 'app-view-comment',
  templateUrl: './view-comment.component.html',
  styles: [
  ]
})
export class ViewCommentComponent implements OnInit {

  private _commentID: string = this._route.snapshot.params['commentID'];

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _httpService: HttpService,
    private _storage: StorageService,
    public query: QueryService
  ) {
    this._getComment();
    this.getVideoInfo();
  }

  public comment: any;
  private _getComment(): void {
    this.comment = <any>this._storage.get(this._commentID);
    if (this.comment) {
      this._storage.remove(this._commentID);
    } else
      this._router.navigate(['/panel/comment'], { queryParams: this.query.params() });
  }

  ngOnInit() { }

  public video_caption: string = "";
  public getVideoInfo(){
    this._httpService.invoke({
      method: 'GET',
      path: `video/load/${this.comment.Video}`
    }).subscribe(
      res => {
        this.video_caption = res.video.name;
      },
      (err) => {
        console.log(err)
      }
    );
  }

  ngOnDestroy() {
  }
}
