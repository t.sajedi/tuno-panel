import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { QueryService } from '../../../services/query.service';
import { StorageService } from '../../../services/storage.service';
import { HttpService } from 'src/app/services/http.service';

interface GetCommentsResponse {
  page: number,
  comments: Array<any>,
  totalPage: number
}

interface Filters {
  page: number,
  limit: number
}
@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styles: [
  ]
})
export class CommentListComponent implements OnInit, OnDestroy {
  public videoID = this._route.snapshot.params['videoID'];

  constructor(
    private _httpService: HttpService,
    private _router: Router,
    private _storage: StorageService,
    private _route: ActivatedRoute,
    public query: QueryService
  ) {
    this.getComments();
  }

  ngOnInit() { }

  public filters: Filters = {
    limit: 10,
    page: 1,
    ...this.query.params()
  }

  public comments: Array<any> = [];
  public totalPages: number = 1;
  public getComments(pageNumber: number = this.filters.page): void {
    this.filters.page = pageNumber;
    this._httpService.invoke({
      method: 'GET',
      path: "comment/list/"+this.videoID,
      query: { ...this.filters }
    }).subscribe(
      res => {
        this.comments = res.comments;
        this.filters.page = res.page;
        this.totalPages = res.totalPage;
        this.query.set(this.filters);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public passToView(comment: any): void {
    this._storage.set(comment.id, comment);
    this._router.navigate(['/panel/comment', comment.id], { queryParams: this.query.params() });
  }

  ngOnDestroy() {
  }
}
