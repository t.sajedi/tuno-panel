import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PanelComponent } from './panel.component';

const routes: Routes = [
  {
    path: '',
    component: PanelComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'videos',
        loadChildren: () => import('../videos/videos.module').then(m => m.VideosModule)
      },
      {
        path: 'actors',
        loadChildren: () => import('../actors/actors.module').then(m => m.ActorsModule)
      },
      {
        path: 'directors',
        loadChildren: () => import('../directors/directors.module').then(m => m.DirectorsModule)
      },
      {
        path: 'genre',
        loadChildren: () => import('../genre/genre.module').then(m => m.GenreModule)
      },
      {
        path: 'tag',
        loadChildren: () => import('../tag/tag.module').then(m => m.TagModule)
      },
      {
        path: 'age',
        loadChildren: () => import('../age/age.module').then(m => m.AgeModule)
      },
      {
        path: 'category',
        loadChildren: () => import('../category/category.module').then(m => m.CategoryModule)
      },
      {
        path: 'data',
        loadChildren: () => import('../category-data/category-data.module').then(m => m.CategoryDataModule)
      },
      {
        path: 'banners',
        loadChildren: () => import('../banner/banner.module').then(m => m.BannerModule)
      },
      {
        path: 'blogs',
        loadChildren: () => import('../blog/blog.module').then(m => m.BlogModule)
      },
      {
        path: 'site',
        loadChildren: () => import('../main-page/main-page.module').then(m => m.MainPageModule)
      },
      {
        path: 'plans',
        loadChildren: () => import('../plans/plans.module').then(m => m.PlansModule)
      },
      {
        path: 'discount',
        loadChildren: () => import('../discount/discount.module').then(m => m.DiscountModule)
      },
      {
        path: 'comment',
        loadChildren: () => import('../comment/comment.module').then(m => m.CommentModule)
      },
      {
        path: 'contact',
        loadChildren: () => import('../contact/contact.module').then(m => m.ContactModule)
      },
      {
        path: 'users',
        loadChildren: () => import('../users/users.module').then(m => m.UsersModule)
      },
      {
        path: 'payments',
        loadChildren: () => import('../payment-setting/payment-setting.module').then(m => m.PaymentSettingModule)
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PanelRoutingModule { }
