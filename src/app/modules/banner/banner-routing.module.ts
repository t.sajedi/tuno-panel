import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BannerListComponent } from './banner-list/banner-list.component';
import { NewBannerComponent } from './new-banner/new-banner.component';

const routes: Routes = [
  {
    path: 'list',
    component: BannerListComponent
  },
  {
    path: 'add',
    component: NewBannerComponent
  },
  {
    path: 'edit/:id',
    component: NewBannerComponent
  },
  {
    path: '',
    component: BannerListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BannerRoutingModule { }
