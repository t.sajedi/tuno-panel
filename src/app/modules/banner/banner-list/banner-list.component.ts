import { Component, OnDestroy, OnInit } from '@angular/core';

import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { QueryService } from '../../../services/query.service';
import { SearchService } from '../../../services/search.service';
import { HttpService } from 'src/app/services/http.service';

interface Filters {
  page: number,
  limit: number,
  sort: string,
  blog: boolean,
  search?: string
}

@Component({
  selector: 'app-banner-list',
  templateUrl: './banner-list.component.html',
  styles: [
  ]
})

export class BannerListComponent implements OnInit, OnDestroy {

  constructor(
    private _httpService: HttpService,
    private _notify: NotifyService,
    public query: QueryService,
    public search: SearchService
  ) {
    this.getBanner();
    search.set("banner", (terms: string): void => {
      this.filters.search = terms;
      this.getBanner(1);
    });
  }

  ngOnInit(): void {
  }

  public filters: Partial<Filters> = {
    limit: 10,
    page: 1,
    sort: 'title',
    blog: false,
    ...this.query.params()
  }

  public banners = [];
  public totalPages: number = 1;
  public getBanner(pageNumber: number = this.filters.page) {
    this.filters.page = pageNumber;
    this._httpService.invoke({
      method: 'GET',
      path: 'banner/list',
      query: { ...this.filters }
    }).subscribe(
      res => {
        this.banners = res.banners || [];
        this.query.set(this.filters);
      },
      (err) => {
        console.log(err)
      }
    )
  }

  public deleteBanner(id: string) {
    this._notify.simpleConfirm(Opr.Del, Ent.Banner, () => {
      this._httpService.invoke({
        method: 'DELETE',
        path: `banner/remove/${id}`,
      }).subscribe(
        res => {
          this.getBanner();
          this._notify.status(Opr.Del, Ent.Banner);
        },
        (err) => {
          console.log(err)
        }
      )
    });
  };
  
  public sort(key: string): void {
    this.filters.sort = this.filters.sort == key ? `-${key}` : key;
    if (!this.banners) { return; }
    this.getBanner(1);
  }
  public order(key: string): string[] {
    return ['sort', this.filters.sort == key ? 'order' :
      this.filters.sort == `-${key}` ? 'reverse' : ''];
  }

  public setFilter(filter?: string, value?: any): void {
    this.filters[filter] = value || (!this.filters[filter]);
    this.getBanner(1);
  }
  
  ngOnDestroy(): void {
  }

}
