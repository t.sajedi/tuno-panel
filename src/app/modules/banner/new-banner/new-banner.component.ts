import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';

import { QueryService } from '../../../services/query.service'
import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-new-banner',
  templateUrl: './new-banner.component.html',
  styles: [
  ]
})
export class NewBannerComponent implements OnInit, OnDestroy {

  public bannerID = this._route.snapshot.params['id'];
  public title: string = (this.bannerID) ? 'banner.create.edit' : 'banner.create.title';

  constructor(
    private _httpService: HttpService,
    private _notify: NotifyService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    public query: QueryService
  ) {
    this._migrateForm();
  }

  ngOnInit(): void {
    this.getVideo();
  }

  public video = [];
  public defaultVideo = [];
  public getVideo() {
    this._httpService.invoke({
      method: 'GET',
      path: 'video/list',
    }).subscribe(
      res => {
        this.video = res.videos || [];
        if (this.bannerID) this.getBannerInfo();
      },
      (err) => {
        if (this.bannerID) this.getBannerInfo();
      }
    )
  }

  public remainingCharcters: number = 50;
  public countCharacters: number;
  public countingCharacters(event) {
    this.countCharacters = event.target.value.length;
    this.remainingCharcters = 50 - this.countCharacters;
  }

  public form: FormGroup;
  private _migrateForm(): void {
    this.form = this._fb.group({
      "title": "",
      "link": "",
      "mobile_image": ["", Validators.required],
      "descktop_image": ["", Validators.required],
      "video": "",
      "description": [""],
      "blog": false
    });
  }
  
  public videoToCreate: string = "";
  public setVideo(event) {
    let id: string = "";
    for (const item of event) {
      id = item.id;
    }
    this.videoToCreate = id;
    this.form.get('video').patchValue(this.videoToCreate);
  }
    
  public getBannerInfo() {
    this._httpService.invoke({
      method: 'GET',
      path: `banner/load/${this.bannerID}`
    }).subscribe(
      res => {
        this.form.controls['title'].patchValue(res.banner.title);
        this.form.controls['link'].patchValue(res.banner.link);
        this.form.controls['description'].patchValue(res.banner.description);
        this.form.controls['video'].patchValue(res.banner.video);
        this.form.controls['mobile_image'].patchValue(res.banner.mobile_image);
        this.form.controls['descktop_image'].patchValue(res.banner.descktop_image);
        this.form.patchValue(res.banner);
        this._makeDefaultDesktopImage(res.banner.descktop_image);
        this._makeDefaultMobileImage(res.banner.mobile_image);
        this.defaultVideo = this.video.filter(video => video.id == res.banner.video);
      },
      (err) => {
        console.log(err)
      }
    )
  }
  
  public setImage(images: string[], prefix: string): void {
    if(!images.length) {
      this.form.controls[prefix].patchValue("");
    }
    else {
      this.form.controls[prefix].patchValue(`/media/${images[0]}`);
    }
  }

  public defaultDesktopImage: any = [];
  private _makeDefaultDesktopImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultDesktopImage = images;
  }

  public defaultMobileImage: any = [];
  private _makeDefaultMobileImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultMobileImage = images;
  }
  
  public formSubmited: boolean = false;
  public submitUrl: string = this.bannerID ? `banner/update/${this.bannerID}` : "banner/add";
  public submit(form) {
    this.formSubmited = true;
    if (form.invalid) return;
    this._httpService.invoke({
      method: this.bannerID ? "PUT" : "POST",
      path: this.submitUrl,
      body: form.value
    }).subscribe(
      res => {
        this._notify.status(this.bannerID ? Opr.Edit : Opr.Create, Ent.Banner);
        this.formSubmited = false;
        this._router.navigate(['/panel/banners'], { queryParams: this.query.params() });
      },
      (err) => {
        console.log(err);
        this.formSubmited = false;
      }
    );
  }

  ngOnDestroy() {
  }
}