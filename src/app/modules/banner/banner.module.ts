import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BannerRoutingModule } from './banner-routing.module';
import { SharedModule } from '../shared/shared.module';

import { NewBannerComponent } from './new-banner/new-banner.component';
import { BannerListComponent } from './banner-list/banner-list.component';


@NgModule({
  declarations: [
    NewBannerComponent,
    BannerListComponent
  ],
  imports: [
    CommonModule,
    BannerRoutingModule,
    SharedModule
  ]
})
export class BannerModule { }
