import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AgeListComponent } from './age-list/age-list.component';
import { CreateAgeComponent } from './create-age/create-age.component';

const routes: Routes = [
  {
    path: 'list',
    component: AgeListComponent
  },
  {
    path: 'add',
    component: CreateAgeComponent
  },
  {
    path: 'edit/:id',
    component: CreateAgeComponent
  },
  {
    path: '',
    component: AgeListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgeRoutingModule { }
