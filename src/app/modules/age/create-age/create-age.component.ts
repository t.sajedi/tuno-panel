import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { QueryService } from '../../../services/query.service';
import { UserService } from '../../../services/user.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-create-age',
  templateUrl: './create-age.component.html',
  styles: [
  ]
})
export class CreateAgeComponent implements OnInit {
  public ageID = this._route.snapshot.params['id'];
  public title: string = (this.ageID) ? 'age.create.edit' : 'age.create.title';

  constructor(
    private _httpService: HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    public query: QueryService,
    public userService: UserService,
  ) {
    this._migrateForm();
    if (this.ageID) this.getAge();
  }

  ngOnInit(): void {}

  public form: FormGroup;
  private _migrateForm(): void {
    this.form = this._fb.group({
      "name": ["", Validators.required],
      "to": 0,
      "from": 0,
      "profile": "",
      "description": "",
      "key": ["", Validators.required]
    });
  }

  public remainingCharcters: number = 300;
  public countCharacters: number;
  public countingCharacters(event) {
    this.countCharacters = event.target.value.length;
    this.remainingCharcters = 300 - this.countCharacters;
  }

  public profile: string = "";
  getActorInfoApi;
  public getAge() {
    this.getActorInfoApi = this._httpService.invoke({
      method: 'GET',
      path: `age/load/${this.ageID}`
    }).subscribe(
      res => {
        this.countCharacters = res.age.description.length;
        this.remainingCharcters = 300 - this.countCharacters;
        this.form.controls['name'].patchValue(res.age.name);
        this.form.controls['profile'].patchValue(res.age.profile);
        this.form.controls['description'].patchValue(res.age.description);
        this.form.patchValue(res.age)
        this._makeDefaultImage(res.age.profile);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public setImage(images: string[], prefix: string): void {
    if(!images.length) {
      this.form.controls[prefix].patchValue("");
    }
    else {
      this.form.controls[prefix].patchValue(`/media/${images[0]}`);
    }
  }

  public defaultImage: any = [];
  private _makeDefaultImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultImage = images;
  }

  public formSubmited: boolean = false;
  public submit(form: FormGroup) {
    this.formSubmited = true;
    if (form.invalid) return;
    var submitUrl = this.ageID ? "age/update/" + this.ageID : "age/add";
    this._httpService.invoke({
      method: this.ageID ? "PUT" : "POST",
      path: submitUrl,
      body: form.value
    }).subscribe(
      res => {
        this.formSubmited = false;
        this._router.navigate(['/panel/age'], { queryParams: this.query.params() });
      },
      (err) => {
        console.log(err);
        this.formSubmited = false;
      }
    );
  }
}
