import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgeListComponent } from './age-list.component';

describe('AgeListComponent', () => {
  let component: AgeListComponent;
  let fixture: ComponentFixture<AgeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
