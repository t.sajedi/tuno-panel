import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgeRoutingModule } from './age-routing.module';
import { SharedModule } from '../shared/shared.module';

import { CreateAgeComponent } from './create-age/create-age.component';
import { AgeListComponent } from './age-list/age-list.component';


@NgModule({
  declarations: [
    CreateAgeComponent,
    AgeListComponent
  ],
  imports: [
    CommonModule,
    AgeRoutingModule,
    SharedModule
  ]
})
export class AgeModule { }
