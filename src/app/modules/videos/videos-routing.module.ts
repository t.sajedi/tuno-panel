import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VideoListComponent } from './video-list/video-list.component';
import { NewVideoComponent } from './new-video/new-video.component';

const routes: Routes = [
  {
    path: 'list',
    component: VideoListComponent
  },
  {
    path: 'add',
    component: NewVideoComponent
  },
  {
    path: 'edit/:id',
    component: NewVideoComponent
  },
  {
    path: '',
    component: VideoListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideosRoutingModule { }
