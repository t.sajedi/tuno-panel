import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

import { Ent, Notify, NotifyService, Opr } from '../../../services/notify.service';
import { SearchService } from '../../../services/search.service';
import { HttpService } from 'src/app/services/http.service';

interface Filters {
  page: number,
  limit: number,
  sort: string,
  search?: string
}
@Component({
  selector: 'app-list',
  templateUrl: './video-list.component.html'
})
export class VideoListComponent implements OnInit {

  constructor(
    private _router: Router,
    private _notify: NotifyService,
    public search: SearchService,
    private _httpService: HttpService,
    public _translate: TranslateService
  ) {
    this.getVideoList();
    search.set("videoList", (terms: string): void => {
      this.filters.search = terms;
      this.getVideoList(1);
    });
  }

    ngOnInit() { }
  
    public filters: Partial<Filters> = {
      limit: 10,
      page: 1,
      search: "",
      sort: '-release_date'
    }
  
    public result: any = [];
    public totalPages: number = 1;
    public getVideoList(pageNumber: number = this.filters.page) {
      this.filters.page = pageNumber;
      this._httpService.invoke({
        method: 'GET',
        path: 'video/list',
        query: { ...this.filters }
      }).subscribe(
        res => {
          this.result = res.videos || [];
          this.totalPages = res.totalPage;
          this.filters.page = res.page;
        },
        (err) => {
          console.log(err)
        }
      );
    }
  
    public videoId: string = "";
    public sendVideoId(id) {
      this.videoId = id;
      this.release_date = "";
    }
  
    public time;
    public release_date;
  
    public publishVideo(event, _id, video) {
      this._httpService.invoke({
        method: 'PUT',
        path: "video/toggle/release/"+_id
      }).subscribe(
        res => {
          this.getVideoList();
        },
        (err) => {
          console.log(err)
        }
      );
    }

    public gotoComment(videoID) {
      this._router.navigate(["/panel/comment/list/"+videoID])
    }
  
    public deleteVideo(videoID: string): void {
      this._notify.simpleConfirm(Opr.Del, Ent.Video, () => {
        this._httpService.invoke({
          method: 'DELETE',
          path: `video/remove/${videoID}`,
        }).subscribe(
          res => {
            this.getVideoList(1);
            this._notify.status(Opr.Del, Ent.Video);
          },
          (err) => {
            console.log(err)
          }
        )
      });
    }
  
    public onImgError(event) {
      event.target.src = 'static/images/no_image.png';
    }
  
    // video demo
    public selectedVideoFile: any = {};
    public videoIsOK: boolean = false;
  
    public videoPlayer: HTMLVideoElement;
    public videoWrapper: HTMLDivElement;
    public videoQuality: any = {
      key: "",
      value: ""
    };
    public videoDuration: string = "";
    public fileSelected: boolean = true;
  
    public videoPath: string = "";
  
    ngOnDestroy() {
    }
}
