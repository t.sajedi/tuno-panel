import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideosRoutingModule } from './videos-routing.module';
import { SharedModule } from "../shared/shared.module";

import { NgPersianDatepickerModule } from 'ng-persian-datepicker';

import { VideoListComponent } from './video-list/video-list.component';
import { NewVideoComponent } from './new-video/new-video.component';


@NgModule({
  declarations: [
    VideoListComponent,
    NewVideoComponent
  ],
  imports: [
    CommonModule,
    VideosRoutingModule,
    SharedModule,
    NgPersianDatepickerModule
  ]
})
export class VideosModule { }
