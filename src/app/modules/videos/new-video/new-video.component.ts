import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'jalali-moment';

import { SearchService } from '../../../services/search.service';
import { HttpService } from 'src/app/services/http.service';

interface ActorFilters {
  page: number,
  limit: number,
  search: string;
  sort: string;
  category?: string;
}

interface imdbFilters {
  imdb: string;
}

@Component({
  selector: 'app-new',
  templateUrl: './new-video.component.html'
})
export class NewVideoComponent implements OnInit {

  public videoID = this._route.snapshot.params['id'];
  public releaseDate = "";
  public title: string = (this.videoID) ? 'video.edit' : 'video.add.title';

  public config = {
    date: {
      dateInitValue: false,
      onSelect: (shamsiDate: string, gregorianDate: string, timestamp: number) => {
        this.form.get("release_date").patchValue(new Date(gregorianDate).toISOString());
      }
    }
  };
  public imdbFilters: Partial<imdbFilters> = {
    imdb: ""
  }
  constructor(
    private _httpService: HttpService,
    public search: SearchService,
    public _translate: TranslateService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
  ) { }

  ngOnInit() {
    this._migrateForm();
    this.getActors();
    this.getDirectors();
    this.getVideos();
    this.getGenre();
    this.getTags();
    this.getAges();
    this.getCategory();
    this.search.set("imdb", (terms: string): void => {
      this.imdbFilters.imdb = terms;
      this.getVideoByIMDB();
    });
  }

  public getVideoByIMDB() {
    this._httpService.invoke({
      method: 'GET',
      path: `video/imdb/${this.imdbFilters.imdb}`
    }).subscribe(
      res => {
        this.form.patchValue(res.data);
        if (res.data.actors) {
          this.actor = res.data.actors || [];
        }
        if (res.data.directors) {
          this.director = res.data.directors || [];
        }
        if (res.data.genre) {
          this.genre = res.data.genre;
        }
        if (res.data.countries) {
          this.countries = res.data.countries;
        }
        if (res.data.languages) {
          this.languages = res.data.languages;
        }
        this.form.get("length").patchValue(res.data.duration_min * 60);
        if (res.data.release_date) {
          this.releaseDate = moment(new Date(res.data.release_date).toDateString()).format("jYYYY-jMM-jDD");
          this.form.get('release_date').patchValue(new Date(res.data.release_date).toISOString());
        }
        this._makeDefaultImagesImdb(res.data);
        this._makeDefaultIMDBImg(res.data.images[0]);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public form: FormGroup;
  private _migrateForm(): void {
    this.form = this._fb.group({
      "mobile_banners": [[]],
      "desktop_banners": [[]],
      "images": [[]],
      "similar_videos": [[]],
      "similar_videos_name": [[]],
      "name": ["", Validators.required],
      "free": false,
      "release_date": new Date().toISOString(),
      "imdb": ["", Validators.required],
      "description": "",
      "genres": [[], Validators.required],
      "tags": [[]],
      "age_range": null,
      "country": [[]],
      "preview": "",
      "language": [[]],
      "length": 0,
      "imdb_rating": 0,
      "category": ["", Validators.required],
      "data": "",
      "director": [[]],
      "actors": [[]],
      "release": false,
      "type": 2,
      "key": [[], Validators.required],
      // seo inputs
      "google_description": "",
      "google_tags": [[]],
      "google_title": "",
      "tump_icon": "",
      "video_urls": this._fb.array([])
    });
  }

  // nested form array
  public videoLinks(): FormArray {
    return this.form.get('video_urls') as FormArray;
  }

  public removeVideoLink(vIndex: number) {
    this.videoLinks().removeAt(vIndex);
  }

  public videoSubtitle(vIndex: number): FormArray {
    return this.videoLinks()
      .at(vIndex)
      .get('subtitles') as FormArray;
  }
  public removeVideoSkill(vIndex: number, sIndex: number) {
    this.videoSubtitle(vIndex).removeAt(sIndex);
  }
  public addVideoSubtitle(vIndex: number, videoSubtitles?: any) {
    this.videoSubtitle(vIndex).push(this.newSubtitle(videoSubtitles));
  }
  public newSubtitle(videoSubtitles?: any): FormGroup {
    return this._fb.group({
      language: [videoSubtitles ? videoSubtitles.language : ''],
      url: [videoSubtitles ? videoSubtitles.url : '']
    });
  }
  public addVideo(i?: number, videoUrls?:any) {
    this.videoLinks().push(this.newVideo(videoUrls));
    if(videoUrls) {
      for(let j = 0; j < videoUrls.subtitles.length; j++) {
        this.addVideoSubtitle(i, videoUrls.subtitles[j]);
      }
    }
  }
  public newVideo(videoUrls?:any): FormGroup {
    return this._fb.group({
      quality: [videoUrls ? videoUrls.quality : '', Validators.required],
      url: [videoUrls ? videoUrls.url : '', Validators.required],
      subtitles: this._fb.array([])
    });
  }
  // actor
  public actors = [];
  public actor: any = [];
  public getActors() {
    this._httpService.invoke({
      method: 'GET',
      path: 'actor/list',
      query: { ...this.actorFilters }
    }).subscribe(
      res => {
        this.actors = res && (res.actores || []);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public actorFilters: Partial<ActorFilters> = {
    limit: 20,
    page: 1,
    search: "",
    sort: "created-at"
  }

  public searchActor(searchValue) {
    this.actorFilters.search = this.converToEnDigit(searchValue);
    this.getActors();
  }

  public actorToCreate: any = [];
  public actorNameToCreate: any = [];
  public setActor(event: [{ name: string, id: string }]) {
    this.form.get("actors").patchValue(event);
  }

  public converToEnDigit(str) {
    return str
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/٠/g, "0")
      .replace(/١/g, "1")
      .replace(/٢/g, "2")
      .replace(/٣/g, "3")
      .replace(/٤/g, "4")
      .replace(/٥/g, "5")
      .replace(/٦/g, "6")
      .replace(/٧/g, "7")
      .replace(/٨/g, "8")
      .replace(/٩/g, "9");
  };

  // directors
  public directors = [];
  public director: any = [];
  public getDirectors() {
    this._httpService.invoke({
      method: 'GET',
      path: 'director/list',
      query: { ...this.directorFilters }
    }).subscribe(
      res => {
        this.directors = res && (res.directores || []);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public directorFilters: Partial<ActorFilters> = {
    limit: 20,
    page: 1,
    search: "",
    sort: "created-at"
  }

  public searchDirector(searchValue) {
    this.directorFilters.search = this.converToEnDigit(searchValue);
    this.getDirectors();
  }

  public setDirector(event: [{ name: string, id: string }]) {
    this.form.get("director").patchValue(event);
  }

  // genres
  public genreFilters: Partial<ActorFilters> = {
    limit: 20,
    page: 1,
    search: "",
    sort: "created-at"
  }

  public searchGenre(searchValue) {
    this.genreFilters.search = this.converToEnDigit(searchValue);
    this.getGenre();
  }
  public genres: Array<string> = [];
  public genre: any = [];
  public getGenre() {
    this._httpService.invoke({
      method: 'GET',
      path: 'genre/list',
      query: { ...this.genreFilters }
    }).subscribe(
      res => {
        this.genres = res && (res.genrees || []);
      },
      (err) => {
        console.log(err)
      }
    );
  }
  public setGenre(event: [{ name: string, id: string }]): void {
    this.form.controls['genres'].patchValue(event);
  }

  // tags
  public tagsFilters: Partial<ActorFilters> = {
    limit: 20,
    page: 1,
    search: "",
    sort: "created-at"
  }

  public searchTag(searchValue) {
    this.tagsFilters.search = this.converToEnDigit(searchValue);
    this.getTags();
  }
  public tags: Array<string> = [];
  public selectedTag: any = [];
  public getTags() {
    this._httpService.invoke({
      method: 'GET',
      path: 'tag/list',
      query: { ...this.tagsFilters }
    }).subscribe(
      res => {
        this.tags = res && (res.tages || []);
      },
      (err) => {
        console.log(err)
      }
    );
  }
  public setTag(event: [{ name: string, id: string }]) {
    this.form.controls['tags'].patchValue(event);
  }
  // category
  public categoryFilters: Partial<ActorFilters> = {
    limit: 20,
    page: 1,
    search: "",
    sort: "created-at"
  }

  public searchCategory(searchValue) {
    this.categoryFilters.search = this.converToEnDigit(searchValue);
    this.getCategory();
  }
  public categories: any = [];
  public selectedCategory: any = [];
  public getCategory() {
    this._httpService.invoke({
      method: 'GET',
      path: 'category/list',
      query: { ...this.categoryFilters }
    }).subscribe(
      res => {
        this.categories = res && (res.categoryes || []);
        if(this.videoID) {
          this.getVideoInfo();
        }
      },
      (err) => {
        if(this.videoID) {
          this.getVideoInfo();
        }
      }
    );
  }
  public setCategory(event: [{ name: string, id: string }]) {
    const idArray: string[] = [];
    for (const item of event) {
      idArray.push(item.id)
    }
    let categoryToCreate = Array.from(new Set(idArray));
    this.form.controls['category'].patchValue(categoryToCreate[0]);
    this.getData(categoryToCreate);
  }
  // data
  public dataFilters: Partial<ActorFilters> = {
    limit: 20,
    page: 1,
    search: "",
    category: "",
    sort: "created-at"
  }

  public searchData(searchValue) {
    this.dataFilters.search = this.converToEnDigit(searchValue);
    this.getData(this.dataFilters.category);
  }
  public data: any = [];
  public selectedData: any = [];
  public getData(category, dataInfo?: any) {
    if (!category || !category.length) { return; }
    this.dataFilters.category = category;
    this._httpService.invoke({
      method: 'GET',
      path: 'category/data/list',
      query: { ...this.dataFilters }
    }).subscribe(
      res => {
        this.data = res && (res.dataes || []);
        for(let i = 0; i < this.data.length; i++) {
          if(this.data[i].id == dataInfo) {
            this.selectedData = [{name: this.data[i].name, id: this.data[i].id}];
          }
        }
      },
      (err) => {
        console.log(err)
      }
    );
  }
  public setData(event: [{ name: string, id: string }]) {
    const idArray: string[] = [];
    for (const item of event) {
      idArray.push(item.id)
    }
    let categoryToCreate = Array.from(new Set(idArray));
    this.form.controls['data'].patchValue(categoryToCreate[0]);
  }
  // age range
  public ageFilters: Partial<ActorFilters> = {
    limit: 20,
    page: 1,
    search: "",
    sort: "created-at"
  }

  public searchAge(searchValue) {
    this.genreFilters.search = this.converToEnDigit(searchValue);
    this.getAges();
  }

  public ages: Array<string> = [];
  public selectedAge: any = [];
  public getAges() {
    this._httpService.invoke({
      method: 'GET',
      path: 'age/list',
      query: { ...this.ageFilters }
    }).subscribe(
      res => {
        this.ages = res && (res.agees || []);
      },
      (err) => {
        console.log(err)
      }
    );
  }
  public setAge(event: [{ name: string, id: string }]) {
    this.form.controls['age_range'].patchValue(event[0]);
  }
  // country
  public countries: Array<string> = [];
  public setCountry(event) {
    this.form.controls['country'].patchValue(event);
  }
  // Key
  public keys: Array<string> = [];
  public setKey(event) {
    this.form.controls['key'].patchValue(event);
  }
  // GTag
  public gtags: Array<string> = [];
  public setGoogleTag(event) {
    this.form.controls['google_tags'].patchValue(event);
  }
  // language
  public languages: Array<string> = [];
  public setLanguage(event) {
    this.form.controls['language'].patchValue(event);
  }
  // related films
  public relatedFilms = [];
  public relatedFilm: any = [];
  public getVideos() {
    this._httpService.invoke({
      method: 'GET',
      path: 'video/list',
      query: { ...this.videoFilters }
    }).subscribe(
      res => {
        this.relatedFilms = res && (res.videos || []);
        this.relatedFilms = this.relatedFilms.filter(film => film.id != this.videoID);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public videoFilters: Partial<ActorFilters> = {
    limit: 20,
    page: 1,
    search: "",
    sort: "created-at"
  }

  public searchVideo(searchValue) {
    this.videoFilters.search = this.converToEnDigit(searchValue);
    this.getVideos();
  }

  public videoToCreate: any = [];
  public videoNameToCreate: any = [];
  public setRelatedFilm(event: [{ name: string, id: string }]) {
    const idArray: string[] = [];
    const nameArray: string[] = [];
    for (const item of event) {
      idArray.push(item.id)
    }
    for (const item of event) {
      nameArray.push(item.name)
    }
    this.videoToCreate = Array.from(new Set(idArray));
    this.form.get("similar_videos").patchValue(this.videoToCreate);
    this.videoNameToCreate = Array.from(new Set(nameArray));
    this.form.get("similar_videos_name").patchValue(this.videoNameToCreate);
  }

  onFileChange(event:any) {
    if (event.target.files && event.target.files[0]) {
        var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = (event:any) => {
                  // Push Base64 string
                  this.images.push(event.target.result); 
                  this.patchValues();
                }
                reader.readAsDataURL(event.target.files[i]);
        }
    }
  }

  onDesktopChange(event:any) {
    if (event.target.files && event.target.files[0]) {
        var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = (event:any) => {
                  // Push Base64 string
                  this.posterDesktop.push(event.target.result); 
                  this.patchValues();
                }
                reader.readAsDataURL(event.target.files[i]);
        }
    }
  }
  onMobileChange(event:any) {
    if (event.target.files && event.target.files[0]) {
        var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = (event:any) => {
                  // Push Base64 string
                  this.posterMobile.push(event.target.result); 
                  this.patchValues();
                }
                reader.readAsDataURL(event.target.files[i]);
        }
    }
  }
  // Patch form Values
  patchValues(){
    this.form.patchValue({
      fileSource: this.images
    });
  }
  // Remove Image
  removeImage(url:any){
    this.images = this.images.filter(img => (img != url));
    this.patchValues();
  }
  removeDesktopImage(url:any){
    this.images = this.images.filter(img => (img != url));
    this.patchValues();
  }
  removeMobileImage(url:any){
    this.images = this.images.filter(img => (img != url));
    this.patchValues();
  }
  images: string[] = [];
  posterDesktop: string[] = [];
  posterMobile: string[] = [];
  public getVideoInfo() {
    this._httpService.invoke({
      method: 'GET',
      path: `video/load/${this.videoID}`
    }).subscribe(
      res => {
        this.form.patchValue(res.video);
        this.actor = res.video.actors || [];
        this.director = res.video.director || [];
        this.genre = res.video.genres;
        this.selectedTag = res.video.tags;
        this.languages = res.video.language;
        this.countries = res.video.country;
        this.keys = res.video.key;
        this.gtags = res.video.google_tags;
        if(res.video.age_range["id"] != "000000000000000000000000") {
          this.selectedAge = [res.video.age_range];
        }
        for(let i = 0; i < this.categories.length; i++) {
          if(this.categories[i].id == res.video.category) {
            this.selectedCategory = [{name: this.categories[i].name, id: this.categories[i].id}];
          }
        }
        this.getData(res.video.category, res.video.data);
        this.releaseDate = moment(new Date(res.video.release_date).toDateString()).format("jYYYY-jMM-jDD");
        this.form.get('release_date').patchValue(new Date(res.video.release_date).toISOString());
        let defaultFilm = [];
        for (let i = 0; i < res.video.similar_videos.length; i++) {
          defaultFilm.push({
            id: res.video.similar_videos[i],
            name: res.video.similar_videos_name[i]
          });
        }
        this.relatedFilm = defaultFilm;
        for(let i = 0; i < res.video.video_urls.length; i++) {
          this.addVideo(i, res.video.video_urls[i]);
        }
        this._makeDefaultImages(res.video);
        this._makeDefaultMobileImages(res.video);
        this._makeDefaultDesktopImages(res.video);
        this._makeDefaultTumpImages(res.video);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public formSubmited: boolean = false;
  public error = "";
  public submitUrl: string = this.videoID ? `video/update/${this.videoID}` : "video/add";
  public updateVideoInfo(form: FormGroup) {
    this.formSubmited = true;
    if(!this.form.get("video_urls").value.length) {
      this.error = "لینک ویدیو نباید خالی باشد";
      return;
    }
    else {
      this.error = "";
    }
    let rate: any = this.form.get("imdb_rating").value;
    this.form.get("imdb_rating").patchValue(parseFloat(rate));
    if (this.form.get("release_date").value == "") {
      this.form.get("release_date").patchValue(new Date().toISOString());
    }
    if (form.invalid) return;
    this._httpService.invoke({
      method: this.videoID ? "PUT" : "POST",
      path: this.submitUrl,
      body: this.form.value
    }).subscribe(
      res => {
        this.formSubmited = false;
        this._router.navigate(['/panel/videos/list']);
      },
      (err) => {
        console.log(err);
        this.formSubmited = false;
      }
    );
  }

  // images
  public defaultImages: any = [];
  private _makeDefaultImages(video): void {
    let images = [];
    if (video.images) {
      video.images.forEach((original: string, i: number): void => {
        let name: string[] = original.split("/");
        images.push({
          original_name: name[name.length - 1],
          path: original,
          thumb: "",
          micro: "",
          big: "",
          mid: ""
        });
      });
    }
    this.defaultImages = images;
  }
  private _makeDefaultImagesImdb(video): void {
    let images = [];
    if (video.images) {
      video.images.forEach((image: any, i: number): void => {
        images.push({
          original_name: image.OriginalName,
          path: image.Path,
          thumb: "",
          micro: "",
          big: "",
          mid: ""
        });
      });
    }
    this.defaultImages = images;
  }
  private _makeDefaultIMDBImg(image): void {
    let images = [];
    if (image) {
      images.push({
        original_name: image.OriginalName,
        path: image.Path,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultDesktopImages = images;
    this.defaultMobileImages = images;
  }
  public setImages(images: string[]): void {
    this.form.controls['images'].patchValue(images);
  }

  public defaultDesktopImages: any = [];
  private _makeDefaultDesktopImages(video): void {
    let images = [];
    if (video.desktop_banners) {
      video.desktop_banners.forEach((original: string, i: number): void => {
        let name: string[] = original.split("/");
        images.push({
          original_name: name[name.length - 1],
          path: original,
          thumb: "",
          micro: "",
          big: "",
          mid: ""
        });
      });
    }
    this.defaultDesktopImages = images;
  }
  public setDesktopImages(images: string[]): void {
    this.form.controls['desktop_banners'].patchValue(images);
  }

  public defaultMobileImages: any = [];
  private _makeDefaultMobileImages(video): void {
    let images = [];
    if (video.mobile_banners) {
      video.mobile_banners.forEach((original: string, i: number): void => {
        let name: string[] = original.split("/");
        images.push({
          original_name: name[name.length - 1],
          path: original,
          thumb: "",
          micro: "",
          big: "",
          mid: ""
        });
      });
    }
    this.defaultMobileImages = images;
  }
  public setMobileImages(images: string[]): void {
    this.form.controls['mobile_banners'].patchValue(images);
  }

  public defaultTumpImage: any = [];
  private _makeDefaultTumpImages(video): void {
    let images = [];
    if (video.tump_icon) {
      images.push({
        original_name: video.tump_icon,
        path: video.tump_icon,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultTumpImage = images;
  }
  public setTumpImages(images: string[]): void {
    this.form.controls['tump_icon'].patchValue(images[0]);
  }

  public activeTab: string = "video";
  public changeTab(activeTab) {
    this.activeTab = activeTab;
  }

  ngOnDestroy() {
  }

}
