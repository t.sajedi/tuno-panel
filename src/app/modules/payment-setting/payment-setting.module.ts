import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentSettingRoutingModule } from './payment-setting-routing.module';
import { PaymentInfoComponent } from './payment-info/payment-info.component';


@NgModule({
  declarations: [
    PaymentInfoComponent
  ],
  imports: [
    CommonModule,
    PaymentSettingRoutingModule
  ]
})
export class PaymentSettingModule { }
