import { Component, OnInit } from '@angular/core';

import { QueryService } from '../../../services/query.service';
import { UserService } from '../../../services/user.service';
import { SearchService } from '../../../services/search.service';
import { Ent, NotifyService, Opr } from '../../../services/notify.service';

import { HttpService } from 'src/app/services/http.service';

interface Filters {
  page: number,
  limit: number,
  sort: string,
  search?: string
}

@Component({
  selector: 'app-director-list',
  templateUrl: './director-list.component.html',
  styles: [
  ]
})
export class DirectorListComponent implements OnInit {

  constructor(
    private _httpService: HttpService,
    private _notify: NotifyService,
    public query: QueryService,
    public userService: UserService,
    public search: SearchService
  ) { }

  ngOnInit(): void {
    this.getDirectors();
    this.search.set("directors", (terms: string): void => {
      this.filters.search = terms;
      this.getDirectors(1);
    });
  }

  public filters: Partial<Filters> = {
    limit: 10,
    page: 1,
    sort: '-created_at',
    ...this.query.params()
  }

  public directores: any[] = [];
  public totalPages: number = 1;
  public getDirectors(pageNumber: number = this.filters.page) {
    this.filters.page = pageNumber;
    this.rowNumber();
    this._httpService.invoke({
      method: 'GET',
      path: 'director/list',
      query: { ...this.filters }
    }).subscribe(
      res => {
        this.directores = res.directores || [];
      this.totalPages = res.totalPage;
      this.filters.page = res.page;
      this.query.set(this.filters);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public rowOffset: number = 0;
  private rowNumber() {
    this.rowOffset = (this.filters.page == 1 ? 0 : (this.filters.page - 1) * this.filters.limit) + 1;
  }

  public deleteDirector(id: string) {
    this._notify.simpleConfirm(Opr.Del, Ent.Director, () => {
      this._httpService.invoke({
        method: 'DELETE',
        path: `director/remove/${id}`,
      }).subscribe(
        res => {
          this.getDirectors(1);
          this._notify.status(Opr.Del, Ent.Director);
        },
        (err) => {
          console.log(err)
        }
      )
    });
  }

  public sort(key: string): void {
    this.filters.sort = this.filters.sort == key ? `-${key}` : key;
    if (!this.directores) { return; }
    this.getDirectors(1);
  }
  public order(key: string): string[] {
    return ['sort', this.filters.sort == key ? 'order' :
      this.filters.sort == `-${key}` ? 'reverse' : ''];
  }

  ngOnDestroy() {
    this.search.remove("directors");
  }

}
