import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { QueryService } from '../../../services/query.service';
import { UserService } from '../../../services/user.service';
import { HttpService } from 'src/app/services/http.service';

interface GenreFilters {
  page: number,
  limit: number,
  search: string;
  sort: string;
}

@Component({
  selector: 'app-new-director',
  templateUrl: './new-director.component.html',
  styles: [
  ]
})
export class NewDirectorComponent implements OnInit {
  public directorID = this._route.snapshot.params['id'];
  public title: string = (this.directorID) ? 'directors.create.edit' : 'directors.create.title';

  constructor(
    private _httpService: HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    public query: QueryService,
    public userService: UserService,
  ) {
    this._migrateForm();
    if (this.directorID) this.getDirector();
  }

  ngOnInit(): void {
    this.getGenres();
  }

  public form: FormGroup;
  private _migrateForm(): void {
    this.form = this._fb.group({
      "name": ["", Validators.required],
      "genre": [],
      "profile": "",
      "description": "",
      "key": ["", Validators.required]
    });
  }

  public remainingCharcters: number = 300;
  public countCharacters: number;
  public countingCharacters(event) {
    this.countCharacters = event.target.value.length;
    this.remainingCharcters = 300 - this.countCharacters;
  }

  public profile: string = "";
  public getDirector() {
    this._httpService.invoke({
      method: 'GET',
      path: `director/load/${this.directorID}`
    }).subscribe(
      res => {
        this.countCharacters = res.director.description.length;
        this.remainingCharcters = 300 - this.countCharacters;
        this.form.controls['name'].patchValue(res.director.name);
        this.form.controls['profile'].patchValue(res.director.profile);
        this.form.controls['description'].patchValue(res.director.description);
        this.selectedGenre = res.director.genre || [];
        this.form.patchValue(res.director);
        this._makeDefaultImage(res.director.profile);
      },
      (err) => {
        console.log(err)
      }
    )
  }

  public setImage(images: string[], prefix: string): void {
    if(!images.length) {
      this.form.controls[prefix].patchValue("");
    }
    else {
      this.form.controls[prefix].patchValue(`/media/${images[0]}`);
    }
  }

  public defaultImage: any = [];
  private _makeDefaultImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultImage = images;
  }

  // genres
  public genres = [];
  public selectedGenre: any = [];
  public getGenres() {
    this._httpService.invoke({
      method: 'GET',
      path: 'genre/list',
      query: { ...this.genreFilters }
    }).subscribe(
      res => {
        this.genres = res.genrees || [];
      },
      (err) => {
        console.log(err)
      }
    )
  }

  public genreFilters: Partial<GenreFilters> = {
    limit: 30,
    page: 1,
    search: "",
    sort: "-created-at"
  }

  public searchGenre(searchValue) {
    this.genreFilters.search = this.converToEnDigit(searchValue);
    this.getGenres();
  }

  public setGenre(event: [{ name: string, id: string }]) {
    this.form.get("genre").patchValue(event);
  }

  public converToEnDigit(str) {
    return str
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/٠/g, "0")
      .replace(/١/g, "1")
      .replace(/٢/g, "2")
      .replace(/٣/g, "3")
      .replace(/٤/g, "4")
      .replace(/٥/g, "5")
      .replace(/٦/g, "6")
      .replace(/٧/g, "7")
      .replace(/٨/g, "8")
      .replace(/٩/g, "9");
  };

  public formSubmited: boolean = false;
  public submit(form: FormGroup) {
    this.formSubmited = true;
    if (form.invalid) return;
    var submitUrl = this.directorID ? "director/update/" + this.directorID : "director/add";
    this._httpService.invoke({
      method: this.directorID ? "PUT" : "POST",
      path: submitUrl,
      body: form.value
    }).subscribe(
      res => {
        this.formSubmited = false;
        this._router.navigate(['/panel/directors'], { queryParams: this.query.params() });
      },
      (err) => {
        console.log(err);
        this.formSubmited = false;
      }
    );
  }

  ngOnDestroy() {
  }
}
