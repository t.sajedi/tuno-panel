import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NewDirectorComponent } from './new-director/new-director.component';
import { DirectorListComponent } from './director-list/director-list.component';

const routes: Routes = [
  {
    path: 'list',
    component: DirectorListComponent
  },
  {
    path: 'add',
    component: NewDirectorComponent
  },
  {
    path: 'edit/:id',
    component: NewDirectorComponent
  },
  {
    path: '',
    component: DirectorListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DirectorsRoutingModule { }
