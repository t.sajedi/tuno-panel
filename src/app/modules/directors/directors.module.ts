import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DirectorsRoutingModule } from './directors-routing.module';
import { SharedModule } from '../shared/shared.module';

import { NewDirectorComponent } from './new-director/new-director.component';
import { DirectorListComponent } from './director-list/director-list.component';


@NgModule({
  declarations: [
    NewDirectorComponent,
    DirectorListComponent
  ],
  imports: [
    CommonModule,
    DirectorsRoutingModule,
    SharedModule
  ]
})
export class DirectorsModule { }
