import { Component, OnInit } from '@angular/core';

import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  constructor(
    private _httpService: HttpService
  ) { }

  ngOnInit(): void {
    this.getReports();
  }

  public totalActors = 0;
  public totalDirectors = 0;
  public totalFilms = 0;
  public totalFreeFilm = 0;
  public totalSerial = 0;
  public totalVideos = 0;
  public totalUsers: number = 0;
  public getReports() {
    this._httpService.invoke({
      method: 'GET',
      path: 'dashboard',
    }).subscribe(
      res => {
        this.totalActors = res.actor_count;
        this.totalDirectors = res.director_count;
        this.totalFilms = res.film_count;
        this.totalFreeFilm = res.free_video_count;
        this.totalSerial = res.serial_count;
        this.totalVideos = res.total_video_count;
        this.totalUsers = res.user_count;
      },
      (err) => {
        console.log(err)
      }
    );
  }


  public totalViews: number = 0;
  public getUsersAndViews() {}

  public allMediaReports;
  public uploadedCount = 0;
  public downloads = 0;
  public uploadedSize_GB = 0;
  public uploadTime_hour = 0;
  public uploadTime_min = 0;
  public uploadTime_sec = 0;
  public getMediaReports() { }


  public allPublishedMediaReports;
  public publishedUploadedCount = 0;
  public publishedUploadedSize_GB = 0;
  public publishedUploadTime_hour = 0;
  public publishedUploadTime_min;
  public publishedUploadTime_sec;
  public getPublishedMediaReports() {}

}
