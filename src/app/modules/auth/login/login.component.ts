import { Component, Renderer2, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { TranslateService } from "@ngx-translate/core";

import { AuthService } from '../../../services/auth.service';
import { User } from '../../../interfaces/user';
import { CustomValidators } from '../../../validators/custom-validators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {

  public loginSubs: Subscription;
  constructor(
    private _renderer: Renderer2,
    private _fb: FormBuilder,
    private _router: Router,
    public translate: TranslateService,
    private _authService: AuthService
  ) {
    this._renderer.removeClass(document.body, 'main-overlay');
    this._renderer.addClass(document.body, 'auth-overlay');
  }

  public form: FormGroup = this._fb.group({
    "input": ["", Validators.compose([Validators.required, Validators.pattern(CustomValidators.numberOnly), Validators.pattern(CustomValidators.cellphone)])],
    "password": ["", Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(16)])]
  })

  ngOnInit(): void {}

  public changeLang(event) {
    let lang = event.target.value;
    this.translate.use(lang);
  }

  public login(form: FormGroup): void {
    for (const field in form.controls)
      form.get(field).markAsTouched();
    if (form.invalid) return;

    this.loginSubs = this._authService.login({
      input: "98" + form.get('input').value.substring(1),
      password: form.get('password').value
    }).subscribe((res: User) => {
      localStorage.setItem('current-user', JSON.stringify(res));
      this._authService.setAuthToken(res.token);
      this._router.navigate(['/panel/dashboard']);
    }, err => { console.log(err)});
  }

  public passInputType: "password" | "text" = "password";
  public togglePasswordVisibility(): void {
    this.passInputType = this.passInputType == "password" ? "text" : "password";
  }

  ngOnDestroy(): void {
    this._renderer.removeClass(document.body, 'auth-overlay');
    this._renderer.addClass(document.body, 'main-overlay');
    if(this.loginSubs) this.loginSubs.unsubscribe();
  }

}
