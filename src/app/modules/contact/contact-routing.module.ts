import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ViewContactComponent } from './view-contact/view-contact.component';

const routes: Routes = [
  {
    path: '',
    component: ContactListComponent,
    data: {
      title: 'contact-us.list.title'
    }
  },
  {
    path: 'view/:contactID',
    component: ViewContactComponent,
    data: {
      title: 'contact-us.list.title'
    }
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactRoutingModule { }
