import { Component, OnInit } from '@angular/core';

import { QueryService } from '../../../services/query.service';

interface Filters {
  page: number,
  limit: number,
}

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styles: [
  ]
})
export class ContactListComponent implements OnInit {

  constructor(
    public query: QueryService
  ) {
    this.getContactUs();
  }

  public filters: Partial<Filters> = {
    limit: 10,
    page: 1,
    ...this.query.params()
  }

  public contactUs: Array<any> = [];
  public totalPages: number = 1;
  public getContactUs(pageNumber: number = this.filters.page) {
    this.filters.page = pageNumber;
  }

  ngOnInit() { }

  public togglePublish(contacts: any): void {
  }

  ngOnDestroy() {
  }
}
