import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DiscountRoutingModule } from './discount-routing.module';
import { SharedModule } from '../shared/shared.module';

import { NewDiscountComponent } from './new-discount/new-discount.component';
import { DiscountListComponent } from './discount-list/discount-list.component';


@NgModule({
  declarations: [
    NewDiscountComponent,
    DiscountListComponent
  ],
  imports: [
    CommonModule,
    DiscountRoutingModule,
    SharedModule
  ]
})
export class DiscountModule { }
