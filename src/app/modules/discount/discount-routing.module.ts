import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DiscountListComponent } from './discount-list/discount-list.component';
import { NewDiscountComponent } from './new-discount/new-discount.component';

const routes: Routes = [
  {
    path: 'list',
    component: DiscountListComponent
  },
  {
    path: 'add',
    component: NewDiscountComponent
  },
  {
    path: 'edit/:id',
    component: NewDiscountComponent
  },
  {
    path: '',
    component: DiscountListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiscountRoutingModule { }
