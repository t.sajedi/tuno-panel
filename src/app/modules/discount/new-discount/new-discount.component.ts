import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { QueryService } from '../../../services/query.service';
import { Ent, NotifyService, Opr } from '../../../services/notify.service';

@Component({
  selector: 'app-new-discount',
  templateUrl: './new-discount.component.html',
  styles: [
  ]
})
export class NewDiscountComponent implements OnInit {

  public title: string = 'discount.create.title';
  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _notify: NotifyService,
    public query: QueryService
  ) { }

  ngOnInit(): void {}

  public form = this._fb.group({
    "name_fa": ["", Validators.required],
    "name_en": "",
    "discount_duration": [0, Validators.required],
    "discount_percent": [0, Validators.required]
  });

  public formSubmited: boolean = false;
  public submit(form) {
    this.formSubmited = true;
    if (form.invalid) return;
 
  }

  ngOnDestroy() {
  }

}
