import { Component, OnDestroy, OnInit } from '@angular/core';

import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { QueryService } from '../../../services/query.service';

interface Filters {
  search?: string,
}

@Component({
  selector: 'app-discount-list',
  templateUrl: './discount-list.component.html',
  styles: [
  ]
})
export class DiscountListComponent implements OnInit {

  constructor(
    public query: QueryService,
    private _notify: NotifyService,
  ) { }

  ngOnInit() {
    // this.getDiscunts();
  }

  public discounts = [];
  public getDiscunts() {
   
  }

  public deleteSubscription(id: string) {
    this._notify.simpleConfirm(Opr.Del, Ent.Discount_code, () => {
    
    });
  }

  ngOnDestroy(): void {
  }

}
