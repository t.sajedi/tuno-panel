import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainPageRoutingModule } from './main-page-routing.module';
import { SharedModule } from '../shared/shared.module';
import { NgPersianDatepickerModule } from 'ng-persian-datepicker';

import { MainPageListComponent } from './main-page-list/main-page-list.component';
import { CreateMainPageComponent } from './create-main-page/create-main-page.component';


@NgModule({
  declarations: [
    MainPageListComponent,
    CreateMainPageComponent
  ],
  imports: [
    CommonModule,
    MainPageRoutingModule,
    SharedModule,
    NgPersianDatepickerModule
  ]
})
export class MainPageModule { }
