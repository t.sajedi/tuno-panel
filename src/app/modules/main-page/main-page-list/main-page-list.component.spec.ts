import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPageListComponent } from './main-page-list.component';

describe('MainPageListComponent', () => {
  let component: MainPageListComponent;
  let fixture: ComponentFixture<MainPageListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainPageListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
