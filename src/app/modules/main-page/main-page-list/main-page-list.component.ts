import { Component, OnDestroy, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { QueryService } from '../../../services/query.service';
import { StorageService } from '../../../services/storage.service';
import { SearchService } from '../../../services/search.service';
import { HttpService } from 'src/app/services/http.service';

interface Filters {
  page: number,
  limit: number,
  sort: string,
  search?: string
}

@Component({
  selector: 'app-main-page-list',
  templateUrl: './main-page-list.component.html',
  styles: [
  ]
})

export class MainPageListComponent implements OnInit, OnDestroy {

  constructor(
    private _httpService: HttpService,
    private _router: Router,
    private _notify: NotifyService,
    public query: QueryService,
    private _storage: StorageService,
    public search: SearchService
  ) {
    this.getSite();
    search.set("site", (terms: string): void => {
      this.filters.search = terms;
      this.getSite(1);
    });
  }

  ngOnInit(): void {
  }

  public filters: Partial<Filters> = {
    limit: 10,
    page: 1,
    sort: '-created_at',
    ...this.query.params()
  }

  public mainpages = [];
  public totalPages: number = 1;
  public getSite(pageNumber: number = this.filters.page) {
    this.filters.page = pageNumber;
    this._httpService.invoke({
      method: 'GET',
      path: 'mainpage/list',
      query: { ...this.filters }
    }).subscribe(
      res => {
        this.mainpages = res.mainpages || [];
        this.query.set(this.filters);
      },
      (err) => {
        console.log(err)
      }
    );
  }

  public deleteSite(id: string) {
    this._notify.simpleConfirm(Opr.Del, Ent.MainPage, () => {
      this._httpService.invoke({
        method: 'DELETE',
        path: `mainpage/remove/${id}`,
      }).subscribe(
        res => {
          this.getSite();
          this._notify.status(Opr.Del, Ent.MainPage);
        },
        (err) => {
          console.log(err)
        }
      )
    });
  };
  
  public bannerId: string = "";
  public sendBannerId(id) {
    this.bannerId = id;
    this.release_date = "";
  }
  
  public time;
  public release_date;
  
  public goToEdit(banner) {
    this._storage.set(banner.id, banner);
    this._router.navigate(['/site/edit', banner.id], { queryParams: this.query.params() }
    );
  }

  public sort(key: string): void {
    this.filters.sort = this.filters.sort == key ? `-${key}` : key;
    if (!this.mainpages) { return; }
    this.getSite(1);
  }
  public order(key: string): string[] {
    return ['sort', this.filters.sort == key ? 'order' :
      this.filters.sort == `-${key}` ? 'reverse' : ''];
  }
  
  ngOnDestroy(): void {
  }

}
