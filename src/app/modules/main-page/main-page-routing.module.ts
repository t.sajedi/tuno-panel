import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainPageListComponent } from './main-page-list/main-page-list.component';
import { CreateMainPageComponent } from './create-main-page/create-main-page.component';

const routes: Routes = [
  {
    path: 'list',
    component: MainPageListComponent
  },
  {
    path: 'add',
    component: CreateMainPageComponent
  },
  {
    path: 'edit/:id',
    component: CreateMainPageComponent
  },
  {
    path: '',
    component: MainPageListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainPageRoutingModule { }
