import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMainPageComponent } from './create-main-page.component';

describe('CreateMainPageComponent', () => {
  let component: CreateMainPageComponent;
  let fixture: ComponentFixture<CreateMainPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateMainPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
