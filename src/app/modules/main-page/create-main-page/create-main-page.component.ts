import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';

import * as moment from 'jalali-moment';

import { QueryService } from '../../../services/query.service'
import { Ent, NotifyService, Opr } from '../../../services/notify.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-create-main-page',
  templateUrl: './create-main-page.component.html',
  styles: [
  ]
})
export class CreateMainPageComponent implements OnInit, OnDestroy {

  public bannerID = this._route.snapshot.params['id'];
  public title: string = (this.bannerID) ? 'site.create.edit' : 'site.create.title';
  public configForm = {
    date: {
      dateInitValue: false,
      onSelect: (shamsiDate: string, gregorianDate: string, timestamp: number) => {
        this.filterForm.get("release_date_from").patchValue(moment(gregorianDate).format("YYYY-MM-DD"));
      }
    }
  };
  public configTo = {
    date: {
      dateInitValue: false,
      onSelect: (shamsiDate: string, gregorianDate: string, timestamp: number) => {
        this.filterForm.get("release_date_to").patchValue(moment(gregorianDate).format("YYYY-MM-DD"));
      }
    }
  };
  public release_date_from = "";
  public release_date_to = "";

  constructor(
    private _httpService: HttpService,
    private _notify: NotifyService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    public query: QueryService
  ) {
    this._migrateForm();
  }

  ngOnInit(): void {
    this.getVideo();
    this.getTags();
    this.getGenre();
    this.getAges();
    this.getActors();
    this.getDirectors();
    this.getCategory();
  }

  public video = [];
  public defaultVideo = [];
  public getVideo() {
    this._httpService.invoke({
      method: 'GET',
      path: "video/list"
    }).subscribe(
      res => {
        this.video = res.videos || [];
        if (this.bannerID) this.getBannerInfo();
      },
      (err) => {
        if (this.bannerID) this.getBannerInfo();
      }
    );
  }

  public remainingCharcters: number = 300;
  public countCharacters: number;
  public countingCharacters(event) {
    this.countCharacters = event.target.value.length;
    this.remainingCharcters = 300 - this.countCharacters;
  }

  public form: FormGroup;
  private _migrateForm(): void {
    this.form = this._fb.group({
      "title": ["" , Validators.required],
      "position": [0, Validators.min(0)],
      "image": false,
      "mobile_image": [""],
      "descktop_image": [""],
      "video": "",
      "filter": ""
    });
  }

  public filterForm: FormGroup = this._fb.group({
    "imdb": "",
    "name": "",
    "tags": "",
    "country": [""],
    "release_date_from": [""],
    "release_date_to": "",
    "genre": "",
    "age_range": "",
    "language": "",
    "length_from": "",
    "length_to": "",
    "director": "",
    "actor": "",
    "type": 2,
    "category": "",
    "data": "",
    "sort": "",
    // "limit": null
  });
  
  public videoToCreate: string = "";
  public setVideo(event) {
    let id: string = "";
    for (const item of event) {
      id = item.id;
    }
    this.videoToCreate = id;
    this.form.get('video').patchValue(this.videoToCreate);
  }
    
  public getBannerInfo() {
    this._httpService.invoke({
      method: 'GET',
      path: `mainpage/load/${this.bannerID}`
    }).subscribe(
      res => {
        this.form.controls['title'].patchValue(res.mainpage.title);
        this.form.controls['video'].patchValue(res.mainpage.video);
        this.form.controls['mobile_image'].patchValue(res.mainpage.mobile_image);
        this.form.controls['descktop_image'].patchValue(res.mainpage.descktop_image);
        this.form.patchValue(res.mainpage)
        this.filterForm.patchValue(res.mainpage.filter);
        this.selectedSort = res.mainpage.filter.sort;
        this.selectedTag = res.mainpage.filter.tags;
        this.genre = res.mainpage.filter.genre;
        this.selectedAge = res.mainpage.filter.age_range;
        this.actor = res.mainpage.filter.actor;
        this.director = res.mainpage.filter.director;
        this.selectedData = res.mainpage.filter.data;
        this.selectedCategory = res.mainpage.filter.category;
        if (res.mainpage.filter.release_date_from) {
          this.release_date_from = moment(new Date(res.mainpage.filter.release_date_from).toDateString()).format("jYYYY-jMM-jDD");
          this.filterForm.get('release_date_from').patchValue(new Date(res.mainpage.filter.release_date_from).toISOString());
        }
        if (res.mainpage.filter.release_date_to) {
          this.release_date_to = moment(new Date(res.mainpage.filter.release_date_to).toDateString()).format("jYYYY-jMM-jDD");
          this.filterForm.get('release_date_to').patchValue(new Date(res.mainpage.filter.release_date_to).toISOString());
        }
        this._makeDefaultDesktopImage(res.mainpage.descktop_image);
        this._makeDefaultMobileImage(res.mainpage.mobile_image);
        this.defaultVideo = this.video.filter(video => video.id == res.mainpage.video);
      },
      (err) => {
        if (this.bannerID) this.getBannerInfo();
      }
    );
  }
  
  public setImage(images: string[], prefix: string): void {
    if(!images.length) {
      this.form.controls[prefix].patchValue("");
    }
    else {
      this.form.controls[prefix].patchValue(`/media/${images[0]}`);
    }
  }

  public defaultDesktopImage: any = [];
  private _makeDefaultDesktopImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultDesktopImage = images;
  }

  public defaultMobileImage: any = [];
  private _makeDefaultMobileImage(image): void {
    let images = [];
    if (image) {
      let name: string[] = image.split("/");
      images.push({
        original_name: name[name.length - 1],
        path: image,
        thumb: "",
        micro: "",
        big: "",
        mid: ""
      });
    }
    this.defaultMobileImage = images;
  }

  // tags
  public tags: Array<string> = [];
  public selectedTag: any = [];
  public getTags() {
    this._httpService.invoke({
      method: 'GET',
      path: "tag/list"
    }).subscribe(
      res => {
        this.tags = res && (res.tages || []);
      },
      (err) => {
      }
    );
  }
  public setTag(event: [{ name: string, id: string }]) {
    this.filterForm.controls['tags'].patchValue(event);
  }

  // sort
  public sorts: Array<{name: string, id: string}> = [
    {name: "نام (صعودی)", id: "name"},
    {name: "نام (نزولی)", id: "-name"},
    {name: "تاریخ ایجاد (صعودی)", id: "created_at"},
    {name: "تاریخ ایجاد(نزولی)", id: "-created_at"}
  ];
  public selectedSort: any = [];
  public setSort(event: [{ name: string, id: string }]) {
    this.filterForm.controls['sort'].patchValue(event);
  }

  // genres
  public genres: Array<string> = [];
  public genre: any = [];
  public getGenre() {
    this._httpService.invoke({
      method: 'GET',
      path: "genre/list"
    }).subscribe(
      res => {
        this.genres = res && (res.genrees || []);
      },
      (err) => {
      }
    );
  }
  public setGenre(event: [{ name: string, id: string }]): void {
    this.filterForm.controls['genre'].patchValue(event);
  }
  // age range
  public ages: Array<string> = [];
  public selectedAge: any = [];
  public getAges() {
    this._httpService.invoke({
      method: 'GET',
      path: "age/list"
    }).subscribe(
      res => {
        this.ages = res && (res.agees || []);
      },
      (err) => {
      }
    );
  }
  public setAge(event: [{ name: string, id: string }]) {
    this.filterForm.controls['age_range'].patchValue(event);
  }
   // actor
   public actors = [];
   public actor: any = [];
   public getActors() {
    this._httpService.invoke({
      method: 'GET',
      path: "actor/list"
    }).subscribe(
      res => {
        this.actors = res && (res.actores || []);
      },
      (err) => {
      }
    );
   }
 
   public searchActor(searchValue) {
     this.getActors();
   }
 
   public actorToCreate: any = [];
   public actorNameToCreate: any = [];
   public setActor(event: [{ name: string, id: string }]) {
     this.filterForm.get("actor").patchValue(event);
   }
 
   public converToEnDigit(str) {
     return str
       .replace(/۰/g, "0")
       .replace(/۱/g, "1")
       .replace(/۲/g, "2")
       .replace(/۳/g, "3")
       .replace(/۴/g, "4")
       .replace(/۵/g, "5")
       .replace(/۶/g, "6")
       .replace(/۷/g, "7")
       .replace(/۸/g, "8")
       .replace(/۹/g, "9")
       .replace(/٠/g, "0")
       .replace(/١/g, "1")
       .replace(/٢/g, "2")
       .replace(/٣/g, "3")
       .replace(/٤/g, "4")
       .replace(/٥/g, "5")
       .replace(/٦/g, "6")
       .replace(/٧/g, "7")
       .replace(/٨/g, "8")
       .replace(/٩/g, "9");
   };
 
   // directors
   public directors = [];
   public director: any = [];
   public getDirectors() {
    this._httpService.invoke({
      method: 'GET',
      path: "director/list"
    }).subscribe(
      res => {
        this.directors = res && (res.directores || []);
      },
      (err) => {
      }
    );
   }
 
  //  public directorFilters: Partial<ActorFilters> = {
  //    limit: 20,
  //    page: 1,
  //    search: "",
  //    sort: "created-at"
  //  }
 
   public searchDirector(searchValue) {
    //  this.directorFilters.search = this.converToEnDigit(searchValue);
     this.getDirectors();
   }
 
   public setDirector(event: [{ name: string, id: string }]) {
     this.filterForm.get("director").patchValue(event);
   }

  // category
  public categories: Array<string> = [];
  public selectedCategory: any = [];
  public getCategory() {
    this._httpService.invoke({
      method: 'GET',
      path: "category/list"
    }).subscribe(
      res => {
        this.categories = res && (res.categoryes || []);
      },
      (err) => {
      }
    );
  }
  public setCategory(event: [{ name: string, id: string }]) {
    this.filterForm.controls['category'].patchValue(event);
    this.getData(event[0].id);
  }
  // data
  public data: Array<string> = [];
  public selectedData: any = [];
  public getData(category) {
    this._httpService.invoke({
      method: 'GET',
      path: "category/data/list",
      query: category
    }).subscribe(
      res => {
        this.data = res && (res.dataes || []);
      },
      (err) => {
      }
    );
  }
  public setData(event: [{ name: string, id: string }]) {
    this.filterForm.controls['data'].patchValue(event);
  }

  public error = "";
  public formSubmited: boolean = false;
  public submitUrl: string = this.bannerID ? `mainpage/update/${this.bannerID}` : "mainpage/add";
  public submit(form) {
    this.formSubmited = true;
    if(form.get("image").value == true && (form.get("mobile_image").value == '' || form.get("descktop_image").value == '' || form.get("video").value == '' || form.get("video").value == '000000000000000000000000')) {
      this.error = "تصاویر و ویدیو نباید خالی باشد";
      return;
    }
    else {
      this.error = "";
    }
    if (form.invalid) return;
    form.get("filter").patchValue(this.filterForm.value);
    this._httpService.invoke({
      method: this.bannerID ? "PUT" : "POST",
      path: this.submitUrl,
      body: form.value
    }).subscribe(
      res => {
        this._notify.status(this.bannerID ? Opr.Edit : Opr.Create, Ent.MainPage);
        this.formSubmited = false;
        this._router.navigate(['/panel/site'], { queryParams: this.query.params() });
      },
      (err) => {
        console.log(err);
        this.formSubmited = false;
      }
    );
  }

  ngOnDestroy() {
  }
}