import { Component, OnInit, SimpleChanges, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { forkJoin } from "rxjs";

import { NotificationsService } from 'angular2-notifications';
import { TranslateService } from '@ngx-translate/core';

import { NotifyService, Opr, Ent } from '../../../services/notify.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'dropzone-images-modal',
  templateUrl: './dropzone-images-modal.component.html',
  styles: []
})
export class DropzoneImagesModalComponent implements OnInit {
  @Input() public selectedImages: any[];
  @Input() public state: 'invisible' | 'visible';
  @Input() public multi: boolean = false;
  @Input() public countLimit: number = 20;
  @Input() public sizeLimit: number = 20480000; // = 20MB
  @Input() public types: ["image"] = ["image"];

  @Output() public closed: EventEmitter<any> = new EventEmitter<any>();
  @Output() public selected: EventEmitter<any[]> = new EventEmitter<any[]>();
  @Output() public removed: EventEmitter<any> = new EventEmitter<any>();

  public dragOvered: boolean = false;
  public illegalFile: boolean = false;

  constructor(
    private _simpleNotify: NotificationsService,
    private _translate: TranslateService,
    private _httpService: HttpService,
    private _notify: NotifyService
  ) { }

  ngAfterViewInit() {
    this.getImages();
  }

  public legalExtentions = ["jpg", "jpeg", "png", "webp"];
  public images: any[] = [];
  public totalPages: number = 1;
  public currentPage: number = 1;
  private _searchTerms: string = "";
  public getImages(pageNumber: number = this.currentPage, searchTerms: string = this._searchTerms): void {
    this._searchTerms = searchTerms;
    this._httpService.invoke({
      method: 'GET',
      path: 'upload',
      query: { limit: 9, page: pageNumber, search: searchTerms }
    }).subscribe(
      res => {
        if (res) {
          this.images = res.gallery.filter(file => this.legalExtentions.includes(this.ext(file.path, file.path.lastIndexOf("."))));
          this.currentPage = res.page;
          this.totalPages = res.total_pages;
        }
        this._setContentTop();
      },
      (err) => {
        console.log(err)
      }
    );
  }

  ngOnInit() { }

  public ext(fileName, ty) {
    return ty === -1 ? undefined : fileName.substring(ty+1); 
  }

  ngOnChanges(changes: SimpleChanges) {
    this._setContentTop();
    this.getImages();
  }

  @ViewChild("header") private _header: ElementRef;
  public contentTop: string = '0';
  private _setContentTop() {
    setTimeout(() => {
      this.contentTop = this._header.nativeElement.offsetHeight + "px";
    });
  }

  public closeModal(): void {
    this.isShowCropper = false;
    this.closed.emit();
  }

  // cropper
  public imageForUpload;

  public uploadImageWithoutCrop() {
    let formData = new FormData();
    this.validatedImageFiles.forEach(imageFile => {
      formData.append("file", imageFile);
    });
    this._httpService.invoke({
      method: "POST",
      path: "upload",
      body: formData
    }).subscribe(
      (res: { files: any[] }) => {
        this.getImages();
        let files = [{id:"", original_name: "", path: ""}];
        files[0].id = res.files[0].ID;
        files[0].original_name = res.files[0].OriginalName;
        files[0].path = res.files[0].Path;
        this.selected.emit(files);
        this._notify.status(Opr.Upload, Ent.Image);
      },
      (err) => {
      
      }
    );
  }

  public scroll(el) {
    if (el.length) {
      el[0].scrollIntoView({
        behavior: "smooth",
        inline: "nearest",
      });
    }
  }

  public isShowCropper: boolean = false;

  public imageName: string = "";
  public toggleSelect(image: any): void {
    if (this.isSelected(image)) {
      this.removed.emit(image);
    }
    else {
      this.selected.emit([image]);
    }
  }

  public isSelected(image: any): boolean {
    return this.selectedImages.map((image: any) => image.original_name).includes(image.original_name);
  }

  public dragEnter(): void {
    this.dragOvered = true;
  }

  public dragLeave(): void {
    this.dragOvered = false;
  }

  public dragOver(event): void {
    event.preventDefault();
    this.dragOvered = true;
    // this.illegalFile = !this._validateImageFiles(event.target.files).length;
  }

  public onDrop(event): void {
    event.preventDefault();
    this.dragOvered = false;
    this.imageName = "";
    this.uploadDropFile(this._validateImageFiles(event.dataTransfer.files));
  }

  public inputChange(event): void {
    this.imageName = event.target.files[0].name;
    this.upload(this._validateImageFiles(event.target.files));
  }

  private _validateImageFiles(imageFiles): any {
    let imageFilesList = [];
    for (let i = 0; i < imageFiles.length; i++) {
      let image = imageFiles[i];
      if (!this.types.includes(image.type.split("/")[0])) {
        let notifyTitle$ = this._translate.get("notify.upload-type-error.title");
        let notifyMessage$ = this._translate.get("notify.upload-type-error.message", { fileName: image.name });
        forkJoin([notifyTitle$, notifyMessage$]).subscribe(data => {
          const [title, message]: string[] = data;
          this._simpleNotify.error(title, message);
        });
        continue;
      }
      if (image.size > this.sizeLimit) {
        let notifyTitle$ = this._translate.get("notify.upload-size-error.title");
        let notifyMessage$ = this._translate.get("notify.upload-size-error.message", { fileName: image.name });
        forkJoin([notifyTitle$, notifyMessage$]).subscribe(data => {
          const [title, message]: string[] = data;
          this._simpleNotify.error(title, message);
        });
        continue;
      }
      imageFilesList.push(image);
    }
    return imageFilesList;
  }

  public validatedImageFiles;
  public upload(validatedImageFiles): void {
    if (!validatedImageFiles.length) return;
    this.validatedImageFiles = validatedImageFiles;
    this.uploadImageWithoutCrop();
  }

  public uploadDropFile(validatedImageFiles) {
    if (!validatedImageFiles.length) return;
    this.validatedImageFiles = validatedImageFiles;
    let formData = new FormData();
    this.validatedImageFiles.forEach(imageFile => {
      formData.append("file", imageFile);
    });
    this._httpService.invoke({
      method: "POST",
      path: "upload",
      body: formData
    }).subscribe(
      (res: { files: any[] }) => {
        this.getImages();
        let files = [{id:"", original_name: "", path: ""}];
        files[0].id = res.files[0].ID;
        files[0].original_name = res.files[0].OriginalName;
        files[0].path = res.files[0].Path;
        this.selected.emit(files);
        this._notify.status(Opr.Upload, Ent.Image);;
      },
      (err) => {
      
      }
    );
  }

  public deleteImage(image: any): void {
    this._notify.simpleConfirm(Opr.Del, Ent.Image, () => {
      let imageID: string[] = image.id;
      this._httpService.invoke({
        method: 'DELETE',
        path: `upload/${imageID}`,
      }).subscribe(
        res => {
          this.removed.emit(image);
          this.getImages();
          this._notify.status(Opr.Del, Ent.Image);
        },
        (err) => {
          console.log(err)
        }
      )
    });
  }

  ngOnDestroy() {
  }
}
