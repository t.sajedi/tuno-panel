import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
// modules
// components
import { SelectInputComponent } from './select-input/select-input.component';
import { InputItemComponent } from './input-item/input-item.component';
import { DropzoneImagesComponent } from './dropzone-images/dropzone-images.component';
import { DropzoneImagesModalComponent } from './dropzone-images-modal/dropzone-images-modal.component';
import { PaginationComponent } from './pagination/pagination.component';
import { TagInputComponent } from './tag-input/tag-input.component';
// pipes
import { SearchPipe } from '../../pipes/search.pipe';
import { JalaliPipe } from '../../pipes/jalali.pipe';
import { SplitPipe } from '../../pipes/make-list.pipe';
import { ExcerptPipe } from '../../pipes/excerpt.pipe';
//directive

@NgModule({
  declarations: [
    SelectInputComponent,
    InputItemComponent,
    DropzoneImagesComponent,
    DropzoneImagesModalComponent,
    PaginationComponent,
    TagInputComponent,
    SearchPipe,
    JalaliPipe,
    SplitPipe,
    ExcerptPipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    SearchPipe,
    JalaliPipe,
    SplitPipe,
    ExcerptPipe,
    SelectInputComponent,
    InputItemComponent,
    TagInputComponent,
    DropzoneImagesComponent,
    DropzoneImagesModalComponent,
    PaginationComponent,
  ]
})
export class SharedModule { }
