import { Component, OnInit } from '@angular/core';

import { UserService } from '../../../services/user.service';
import { UserList } from '../../../interfaces/user-list';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html'
})
export class UserListComponent implements OnInit {
  public query;
  public p: number;
  public getU: Subscription;
  public changeStatusU: Subscription;
  public removeU: Subscription;
  constructor(
    private _userService: UserService
  ) { }

  ngOnInit(): void {
    this.showUsers();
  }

  public u: UserList;
  public users = [];
  public showSpinner: boolean = true;
  public showUsers(): void {
    this.getU = this._userService.getUsers().subscribe((res: {users: UserList}) => {
      this.showSpinner = false;
      this.u = { ...res.users };
      this.p = 1;
      this.users = [];
      for(const key in this.u)
        this.users.push(this.u[key]);
      }, err => {
        this.showSpinner = false;
        console.log(err);
      }
    );
  }

  public order: string = 'username';
  public reverse: boolean = false;
  public setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }

  public changeUserStatus(id: string, body) {
    this.changeStatusU = this._userService.changeUserStatus(id, body).subscribe((res: {users: UserList}) => {
      this.showSpinner = false;
      this.showUsers();
      }, err => {
        this.showSpinner = false;
        console.log(err);
      }
    );
  }

}
