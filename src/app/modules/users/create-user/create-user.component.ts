import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { TranslateService } from "@ngx-translate/core";
import { NotificationsService } from "angular2-notifications";

import { UserService } from '../../../services/user.service';
import { CustomValidators } from '../../../validators/custom-validators';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html'
})
export class CreateUserComponent implements OnInit {

  public title = "user.create"
  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _translate: TranslateService,
    private _notify: NotificationsService,
    private _userService: UserService
  ) { }

  public form = this._fb.group({
    "input": ["", Validators.compose([Validators.required])],
    "mobile": [""],
    "fullname": ["", Validators.required],
    "password": ["", Validators.compose([Validators.required,Validators.pattern(CustomValidators.password)])]
  });

  ngOnInit(): void {}

  public isAdmin: boolean = false;
  public adminToggle(event) {
    this.isAdmin = event.target.checked;
    this.changeValidity();
  }

  public changeValidity() {
    if (this.isAdmin) {
      this.form.controls.input.setValidators([Validators.nullValidator]);
      this.form.controls.mobile.setValidators([Validators.required, Validators.pattern(CustomValidators.numberOnly), Validators.pattern(CustomValidators.cellphone)]);
    } else {
      this.form.controls.input.setValidators([Validators.required]);
      this.form.controls.mobile.setValidators([Validators.nullValidator]);
    }
    this.form.controls.input.updateValueAndValidity();
    this.form.controls.mobile.updateValueAndValidity();
  }

  public createUserAdmin: Subscription;
  public editUser: Subscription;
  public submit(form: FormGroup) {
    // for(const key in form) {
    //   form[key].markA
    // }
    form.markAllAsTouched();
    if(form.invalid) {
      return;
    }
    if(this.isAdmin) {
      let data = {
        "mobile": form.get("mobile").value,
        "fullname": form.get("fullname").value,
        "password": form.get("password").value,
      };
      this.createUserAdmin = this._userService.setUserAdmin(data).subscribe(() => {
        this._translate.get("notify.success.user-create").subscribe(notify => {
          this._notify.success(notify.title, notify.text);
        });
        this._router.navigate(['/panel/users/list']);
      });
    }

    if(!this.isAdmin) {
      let data = {
        "input": form.get("input").value,
        "fullname": form.get("fullname").value,
        "password": form.get("password").value
      };
      this.editUser = this._userService.setUser(data).subscribe(() => {
        this._translate.get("notify.success.user-create").subscribe(notify => {
          this._notify.success(notify.title, notify.text);
        });
        this._router.navigate(['/panel/users/list']);
      });
    }
  }

}
