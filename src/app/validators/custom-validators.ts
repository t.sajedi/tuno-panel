export class CustomValidators {

  static email: RegExp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  static cellphone: RegExp = /^09[0|1|2|3|9][0-9]{8}$/i;
  static numberOnly: RegExp = /^[0-9]*$/i;
  static password: RegExp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

}
