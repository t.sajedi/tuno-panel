import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';

import { NotificationsService } from 'angular2-notifications';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';

@Injectable()
export class ReqInterceptor implements HttpInterceptor {

  constructor(
    private _router: Router,
    private _translate: TranslateService,
    private _notify: NotificationsService,
    private _userService: UserService,
    private _authService: AuthService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = this._authService.getAuthToken();

    if (!req.url.includes("static"))
      req = req.clone({
        url: `/api/${req.url}`
      });

    if (token) {
        // If we have a token, we set it to the header
        req = req.clone({
           setHeaders: {Authorization: `${token}`}
        });
     }

    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        // if (event instanceof HttpResponse) {
          return event instanceof HttpResponse ? event : null;
        // }
        // else {
          // return ;
        // }
      }),
      catchError(err => {
        switch (err.status) {
          case 400:
            if (err.error.code == 4002) {
              this._translate.get("error.4002").subscribe(notify => {
                this._notify.error(notify.title, notify.text);
              });
            }
            if (err.error.code == 4004) {
              this._translate.get("error.4004").subscribe(notify => {
                this._notify.error(notify.title, notify.text);
              });
            }
            if (err.error.code == 400032 && err.error.error == "wrong password") {
              this._translate.get("error.400032").subscribe(notify => {
                this._notify.error(notify.title, notify.text);
              });
            }
            if (err.error.code == 40012 && err.error["debug_msg"] == "duplicate imdb") {
              this._translate.get("error.40012-imdb").subscribe(notify => {
                this._notify.error(notify.title, notify.text);
              });
            }
            if (err.error.code == 40012 && err.error["debug_msg"] != "duplicate position" && err.error["debug_msg"] != "duplicate imdb") {
              this._translate.get("error.40012").subscribe(notify => {
                this._notify.error(notify.title, notify.text);
              });
            }
            if (err.error.code == 40012 && err.error["debug_msg"] == "duplicate position") {
              this._translate.get("error.40012-position").subscribe(notify => {
                this._notify.error(notify.title, notify.text);
              });
            }
            if (err.error.code == 5001) {
              this._translate.get("error.5001").subscribe(notify => {
                this._notify.error(notify.title, notify.text);
              });
            }
            break;
          case 401:
            this._translate.get("error.401").subscribe(notify => {
              this._notify.error(notify.title, notify.text);
            });
            this._userService.ejectUser();
            break;
          case 403:
            if (err.error.code == 40022 && err.error.error == "permission denied") {
              this._translate.get("error.40022").subscribe(notify => {
                this._notify.error(notify.title, notify.text);
              });
            }
            this._router.navigate(['/panel']);
            break;
          case 404:
            this._translate.get("error.404").subscribe(notify => {
              this._notify.error(notify.title, notify.text);
            });
            break;
          case 504:
            this._translate.get("error.504").subscribe(notify => {
              this._notify.error(notify.title, notify.text);
            });
            break;
          default:
            break;
        }
        return observableThrowError(err);
      })
    );
  }
}
