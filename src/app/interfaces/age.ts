export interface Age {
  created_at: string;
  created_by: string;
  description: string;
  id: string;
  name: string;
  profile: string;
  from: number;
  to: number;
}