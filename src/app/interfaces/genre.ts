export interface Genre {
  created_at: string;
  created_by: string;
  description: string;
  id: string;
  name: string;
  profile: string;
}