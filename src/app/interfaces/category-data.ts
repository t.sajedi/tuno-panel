export interface CategoryData {
  created_at: string;
  description: string;
  id: string;
  name: string;
  posters: string[];
  profile: string;
  type: number;
  key: string;
}