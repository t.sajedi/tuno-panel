export interface UserList {
  createdAt: string;
  email: string;
  firstLogin: boolean;
  fullname: string;
  id: string;
  isAdmin: boolean;
  mobile: string;
  suspend: boolean;
}
