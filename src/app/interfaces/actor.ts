export interface Actors {
  created_at: string;
  created_by: string;
  description: string;
  id: string;
  name: string;
  profile: string;
  key: string;
}