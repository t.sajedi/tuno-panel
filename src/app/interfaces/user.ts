export interface User {
  id: number;
  email: string;
  is_admin: boolean;
  phone: string;
  username: string;
  fullname: string;
  token: string;
}
