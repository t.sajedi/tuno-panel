import { Component, Renderer2, OnDestroy } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {

  private $onLangChange;
  public options = {};

  constructor(
    private _translate: TranslateService,
    private _renderer: Renderer2,
    private _userService: UserService
  ) {
    this._renderer.addClass(document.body, 'main-overlay');
    let defaultLanguage: string = 'fa' || _translate.getBrowserLang();
    _translate.setDefaultLang(defaultLanguage);

    this.$onLangChange = _translate.onLangChange.subscribe(data => {
      localStorage.setItem('lang', data.lang);
      this._toggleLang(data.lang);
    });
    _translate.use(localStorage.getItem('lang') || defaultLanguage);

    // _userService.checkCookie();
  }

  ngOnInit() {
    this._userService.getCurrentUser();
  }

  private _toggleLang(lang: string): void {
    let options = {
      timeOut: 5000,
      maxStack: 3
    };
    if(lang == 'fa' ){
      this._renderer.addClass(document.body, 'rtl');
      this._renderer.addClass(document.body, 'menu-on-right');
      this.options = {
        position: ['bottom', 'right'],
        ...options
      }
    } else {
      this._renderer.removeClass(document.body, 'rtl');
      this._renderer.removeClass(document.body, 'menu-on-right');
      this.options = {
        position: ["bottom", "left"],
        ...options
      };
    }
  }

  ngOnDestroy(): void {
    this._renderer.removeClass(document.body, 'main-overlay');
  }

}
