import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  @Output() public sidebarToggle: EventEmitter<void> = new EventEmitter<void>();
  public pageTitle: string = "dashboard.~";
  constructor(public translate: TranslateService, public userService: UserService) { }

  ngOnInit(): void { }


  public onLang(language: string) {
    this.translate.use(language);
  }

  public logout() {
    this.userService.logout();
  }
}
