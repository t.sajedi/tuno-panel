import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {

  constructor(
    private _userservice: UserService
  ) { }

  ngOnInit(): void {}

  public appVersion = { value: "0.0.1" };
  @Output() public sidebarToggle: EventEmitter<void> = new EventEmitter<void>();
  public isAdmin() {
    return this._userservice.user().is_admin;
  }

}
